<?php

return [
    'community_id' => env('VKONTAKTE_COMMUNITY_ID',192649044),

    'app_id' => env('VKONTAKTE_APP_ID',"your-app-id"),
    'app_secret' => env('VKONTAKTE_APP_SECRET',"your-app-secret"),

];
