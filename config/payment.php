<?php


return [
    'merchant_id' => env("PAYMENT_MERCHANT_ID", 0),
    'secret_out' => env("PAYMENT_OUT_SECRET", "secret1"),
    'secret_in' => env("PAYMENT_IN_SECRET", "secret2"),
];
