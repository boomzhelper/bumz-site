<?php

return [
    'login_key' => env('LOGIN_KEY', 'QY-16-WAN-0668-2555555-7ROAD-dandantang-trminhpc773377'),
    'server_name' => env('GAME_SERVER_NAME', 'Your server name'),
    'login_server_name' => env('LOGIN_SERVER_NAME', 'your login\'s server name'),

    'config_path' => env('GAME_CONFIG', 'https://patch-to-your.config'),
    'flash' => env('GAME_FLASH', 'https://patch-to-your.flash'),
    'request' => env('GAME_ENDPOINT', 'https://patch-to-your.request'),
];
