<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('home');
Route::get('/documents', 'IndexController@documents')->name('documents');
Route::get('/documents/policy', 'IndexController@policy')->name('policy');
Route::get('/documents/cookie', 'IndexController@cookie')->name('cookie');
Route::get('/documents/rules', 'IndexController@rules')->name('rules');
Route::get('/posts', 'PostController@index')->name('news');
Route::get('/post/{id}-{path}', 'PostController@expand')->name('post');

Route::post('/donate/notification', 'PaymentController@receiveNotification');

//Route::get('/donate/test', 'PaymentController@test');

Route::get('/dd6e027ad9fbf25e2285b7c422b8656b.html', function () {
})->name('lolkek');

Route::prefix('validation')->group(function () {
    Route::get('registration', 'ValidationController@handle')->name('validate-registration');
});

Route::get('/registration', 'UserController@create');
Route::post('/user/login', 'Auth\LoginController@login');
Route::resource('user', 'UserController')->only([
    'store'
]);

Route::get('/vkontakte/game', 'GameController@createGameViaVkontakte')->middleware('character')->name('game');

Route::middleware('auth')->group(function () {
    Route::get('/settings', 'SettingsController@show')->name('settings');
    Route::post('/settings', 'SettingsController@save')->name('settings-save');

    Route::get('/donate', 'PaymentController@show')->name('donate');
    Route::post('/donate', 'PaymentController@redirectToPay');
    Route::post('/donate/transit', 'PaymentController@transitToGame')->name('transit');

    Route::get('/character/create', 'CharactersController@show')->name('create-character');
    Route::post('/character/create', 'CharactersController@create');


    Route::get('/registration-completing', 'Auth\RegisterController@showRegistrationCompleteForm')->name('registration-complete');
    Route::post('/registration-completing', 'Auth\RegisterController@completingRegistration')->name('registration-completing');

    Route::group(['middleware' => ['level:10']], function () {
        Route::prefix('admin')->group(function () {
            Route::get('/slides/create', 'AdminController@showSlideCreate')->middleware('permission:slide.add')->name('slide.create');
            Route::post('/slides/create', 'SliderController@create')->middleware('permission:slide.add');
            Route::get('/posts/create', 'AdminController@showPostCreate')->middleware('permission:post.add')->name('admin.post-create');
            Route::post('/posts/create', 'PostController@create')->middleware('permission:post.add');
        });
    });

    Route::group(['middleware' => ['level:10']], function () {
        Route::prefix('telegram-bot')->group(function () {
            Route::get('/activate', 'AdminController@activateTelegramBot')->middleware('permission:telegram.activate')->name('admin.telegram.activate');
        });
    });

});

Route::any('1094810348:AAE7Xu6rE7nHPM7KHAyh96xna892CK6vGz8/webhook', 'TelegramController@handle');



Route::get('events', 'EventController@view')->name('events');

Route::prefix('2fa')->group(function () {
    Route::get('enable', 'Google2FAController@enableTwoFactor')->middleware('auth');
    Route::post('enable', 'Google2FAController@confirmTwoFactor')->middleware('auth')->name('confirmTwoFactor');
    Route::get('disable', 'Google2FAController@disablePageTwoFactor')->middleware('auth');
    Route::post('disable', 'Google2FAController@disableTwoFactor')->middleware('auth')->name('disableTwoFactor');
    Route::get('validate', 'Auth\LoginController@getValidateToken')->name('LoginVia2fa');
    Route::post('validate', ['middleware' => 'throttle:5', 'uses' => 'Auth\LoginController@postValidateToken']);
});



Route::prefix('social')->group(function () {
    Route::get('{driver}/redirect', 'SocialController@redirect')->name('social-auth');
    Route::get('{driver}/callback', 'SocialController@callback');
});

Route::prefix('user-old')->group(function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');

    Route::post('register', 'Auth\RegisterController@register')->name('registration');

    Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
    Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

    Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
});

