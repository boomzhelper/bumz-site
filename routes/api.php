<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix('user')->group(function () {
    Route::get('/create', function () {
        \App\Models\User::create([
            'name' => "random-".rand(199,999),
            'email' => "example-".rand(199,999)."@gad.ru",
            'password' => Hash::make('CfgAim12'),
        ]);
    });
});


Route::get('/slides', 'SliderController@load');
