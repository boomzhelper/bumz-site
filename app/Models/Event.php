<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Event
 *
 * @property int $id
 * @property string $title
 * @property string $short_description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $image
 * @property int $size
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\News\Action[] $actions
 * @property-read int|null $actions_count
 * @property-read \App\Models\Page $page
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereShortDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Event extends Model
{
    /**
       * constant int NORMAL
       * constant int LARGE
    */
    public const NORMAL = 0;
    public const LARGE = 1;

    protected $fillable = [
        'title', 'short_description', 'image', 'size', 'block_size'
    ];

    public function page()
    {
        return $this->hasOne('App\Models\Page');
    }

    public function actions()
    {
        return $this->hasMany('App\Models\News\Action');
    }
}
