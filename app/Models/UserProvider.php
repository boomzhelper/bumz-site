<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserProvider
 *
 * @property int $id
 * @property int $user_id
 * @property string $provider
 * @property string $provider_id
 * @property string|null $image
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserProvider newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserProvider newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserProvider query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserProvider whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserProvider whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserProvider whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserProvider whereProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserProvider whereProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserProvider whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserProvider whereUserId($value)
 * @mixin \Eloquent
 */
class UserProvider extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'provider', 'provider_id', 'image'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    protected $table = 'user_providers';
}
