<?php

namespace App\Models\News;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\News\Action
 *
 * @property int $id
 * @property int $post_id
 * @property string $type
 * @property int $enabled
 * @property int $guest_access
 * @property int $guest_visible
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $event_id
 * @property int $page_id
 * @property-read \App\Models\News\Post $post
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Action newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Action newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Action query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Action whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Action whereEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Action whereEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Action whereGuestAccess($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Action whereGuestVisible($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Action whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Action wherePageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Action wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Action whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Action whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Action extends Model
{
    public const COMMENTS = 'comments';

    protected $fillable = [
        'post_id', 'event_id',  'type', 'enabled', 'guest_access', 'guest_visible', 'action'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|null
     */
    public function comments()
    {
        if($this->type == Action::COMMENTS)
            return $this->hasMany('App\Models\News\Comment');
        return null;
    }

    public function event()
    {
        return $this->belongsTo('App\Models\Event');
    }

    public function post()
    {
        return $this->belongsTo('App\Models\News\Post');
    }
}
