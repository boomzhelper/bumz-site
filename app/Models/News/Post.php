<?php

namespace App\Models\News;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\News\Post
 *
 * @property int $id
 * @property string $title
 * @property string $short_description
 * @property string $full_description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $image
 * @property string $path
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\News\Action[] $actions
 * @property-read int|null $actions_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Post newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Post newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Post query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Post whereFullDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Post whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Post wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Post whereShortDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Post whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Post whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Post extends Model
{
    protected $fillable = [
        'title', 'path', 'short_description', 'full_description', 'image', 'size'
    ];

    public function actions()
    {
        return $this->hasMany('App\Models\News\Action');
    }
}
