<?php

namespace App\Models\News;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\News\Comment
 *
 * @property int $id
 * @property int $action_id
 * @property int $user_id
 * @property string $content
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\News\Action $action
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Comment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Comment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Comment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Comment whereActionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Comment whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Comment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\Comment whereUserId($value)
 * @mixin \Eloquent
 */
class Comment extends Model
{
    protected $fillable = [
        'action_id', 'user_id', 'content'
    ];

    public function action()
    {
        return $this->belongsTo('App\Models\News\Action');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
