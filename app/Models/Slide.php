<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Slide
 *
 * @property int $id
 * @property string $url
 * @property string $link
 * @property string $alt
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slide newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slide newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slide query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slide whereAlt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slide whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slide whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slide whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slide whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slide whereUrl($value)
 * @mixin \Eloquent
 */
class Slide extends Model
{
    protected $fillable = [
        'url', 'link', 'alt'
    ];
}
