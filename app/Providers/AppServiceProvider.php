<?php

namespace App\Providers;

use Auth;
use Blade;
use Illuminate\Support\ServiceProvider;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerBladeComponents();
    }

    public function registerBladeComponents()
    {
        Blade::component('modules.two-factor', 'TwoFactor');
        Blade::component('modules.posts', 'Posts');
    }
}
