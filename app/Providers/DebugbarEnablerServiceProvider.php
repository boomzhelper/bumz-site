<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use SocialiteProviders\Manager\SocialiteWasCalled;

class DebugbarEnablerServiceProvider extends ServiceProvider
{

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        $debugbarRoutes = collect(app('router')->getRoutes()->getRoutesByName())
            ->filter(function ($value, $key) {
                return strpos($key, 'debugbar') !== false;
            });

        foreach ($debugbarRoutes as $route) {
            $action = $route->getAction();
            array_unshift($action['middleware'], 'web');
            $route->setAction($action);
        };
    }
}
