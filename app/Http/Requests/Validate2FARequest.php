<?php

namespace App\Http\Requests;

use App\Rules\UsedToken;
use App\Rules\ValidToken;
use Auth;
use Cache;
use Crypt;
use Google2FA;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory as ValidatonFactory;

class Validate2FARequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'totp' => ['bail', 'required', 'digits:6', new ValidToken, new UsedToken],
        ];
    }
}
