<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreSlideRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->hasPermission('slide.add');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slide' => [
                'required',
                'mimes:jpeg,png,webp',
                //'dimensions:width=650,height=350',
            ],
            'name' => 'string|required|max:255',
            'link' => 'string|required|max:255'
        ];
    }
}
