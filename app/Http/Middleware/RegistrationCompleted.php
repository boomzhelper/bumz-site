<?php

namespace App\Http\Middleware;

use Closure;
use Route;

class RegistrationCompleted
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();
        if($user)
        {
            if(!$user->registrationCompleted())
                if(Route::currentRouteName() != 'registration-complete')
                    if(Route::currentRouteName() != 'registration-completing')
                        if(Route::currentRouteName() != 'logout')
                            if(Route::currentRouteName() != 'validate-registration')
                                return redirect('registration-completing');
        }
        return $next($request);
    }
}
