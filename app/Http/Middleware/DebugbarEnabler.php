<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class DebugbarEnabler
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {
            $user = Auth::user();
//            if($user->canDebugAccessDebugbar()) {
//                config(['debugbar.enabled' => true]);
//            }
            if($user->canDebugAccessDebugbar()) {
                \Debugbar::enable();
            }
        }
        return $next($request);
    }
}
