<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use DB;

class RedirectIfHaventCharacter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            if (DB::connection('ddtank')->table('Sys_Users_Detail')->where('UserName', Auth::user()->name)->doesntExist()) {
                return redirect(route('create-character'));
            }
        }

        return $next($request);
    }
}
