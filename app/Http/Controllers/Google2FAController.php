<?php

namespace App\Http\Controllers;

use App\Http\Requests\Validate2FARequest;
use Crypt;
use Google2FA;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use \ParagonIE\ConstantTime\Base32;
use SEO;

class Google2FAController extends Controller
{
    use ValidatesRequests;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    /**
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function enableTwoFactor(Request $request)
    {
        SEO::setTitle('Двухфакторная аутентификация');
        //get user
        $user = $request->user();

        //generate new secret

        $secret = session('secretKey') ?? $request->session()->get('secretKey', function () {
                $key = $this->generateSecret();
                session(['secretKey' => $key]);
                return $key;
            });


        //encrypt and then save secret
        //$user->google2fa_secret = Crypt::encrypt($secret);
        //$user->save();

        //generate image for QR barcode
        $imageDataUri = \Cache::remember("{$secret}-image", 120, function () use($user, $request, $secret) {
            return Google2FA::getQRCodeInlineV2(
                $request->getHttpHost(),
                $user->email,
                $secret
            );
        });

        return view('2fa/enable-two-factor', ['image' => $imageDataUri, 'secret' => $secret]);
    }

    public function confirmTwoFactor(Validate2FARequest $request)
    {
        $user = $request->user();

        $user->google2fa_secret = session('secretKey');
        $user->save();

        return redirect()->to('/')->with('success', ['tfa.enabled']);
    }

    /**
     *
     * @param Validate2FARequest $request
     * @return \Illuminate\Http\Response
     */
    public function disableTwoFactor(Validate2FARequest $request)
    {
        $user = $request->user();

        $user->google2fa_secret = null;
        $user->save();

        return redirect()->to('/')->with('success', ['tfa.disabled']);
    }

    public function disablePageTwoFactor()
    {
        SEO::setTitle('Двухфакторная аутентификация');
        return view('2fa/disable-two-factor');
    }


    /**
     * Generate a secret key in Base32 format
     *
     * @return string
     * @throws \Exception
     */
    private function generateSecret()
    {
        $randomBytes = random_bytes(10);

        return Base32::encodeUpper($randomBytes);
    }
}
