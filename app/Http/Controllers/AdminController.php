<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Illuminate\Http\Request;
use Telegram;
use Validator;

class AdminController extends Controller
{
    public function showSlideCreate()
    {
        return view('admin.slide-create');
    }

    public function showPostCreate()
    {
        return view('admin.post-create');
    }

    public function activateTelegramBot()
    {
        Telegram::setWebhook(["url" => url(config('telegram.bots.mybot.webhook_url'))]);

        sleep(1);
        try {
            Telegram::bot()->setChatTitle(['chat_id' => config('telegram.bots.mybot.groupId'), 'title' => config('telegram.bots.mybot.groupName')]);
        } catch (Telegram\Bot\Exceptions\TelegramSDKException $e) {
            return back()->withErrors([$e->getMessage()]);
        }

        sleep(1);
        try {
            Telegram::bot()->setChatDescription(['chat_id' => config('telegram.bots.mybot.groupId'), 'description' => config('telegram.bots.mybot.groupDesc')]);
        } catch (Telegram\Bot\Exceptions\TelegramSDKException $e) {
            return back()->withErrors([$e->getMessage()]);
        }
        //$updates = Telegram::setWebhook(["url" => url(config('telegram.bots.mybot.webhook_url'))]);

        return back()->with('success', ['admin.telegram.activated']);
    }
}
