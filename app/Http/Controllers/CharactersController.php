<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Illuminate\Http\Request;
use Validator;

class CharactersController extends Controller
{
    public function show()
    {
        return view('character.create');
    }

    public function create(Request $request)
    {
        if(!$request->has('sex'))
            return back()->withErrors(['character.sex.required']);
        if(!$request->has('nickname'))
            return back()->withErrors(['character.nickname.required']);

        $preg = "[\"\_\+\-\=\{\}\[\]\:\"\|\;\'\\\?\,\.\/ \~\！\@\#\￥\%\…\…\&\*\（\）\：\“\|\《\》\？\{\}\—\—\+\-\=\·\【\】\；\‘\、\。\、\，\(\)]";
        $fullyPreg = "/{$preg}/u";

        $validator = Validator::make($request->all(), [
            'sex' => 'required',
            'nickname' => ['required', "not_regex:$fullyPreg"],
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $sex = $request->input('sex') == "male";
        $nickname = $request->input('nickname');
        $username = Auth::user()->name;
        $email = Auth::user()->email;
        $password = "";
        $userId = 0;

        if (DB::connection('ddtank')->table('Sys_Users_Detail')->where('NickName', $nickname)->exists()) {
            return back()
                ->withErrors(['character.nickname.exist'])
                ->withInput();
        }

        if($nickname != null) {
            DB::connection('ddtank')->update('exec SP_Users_Active ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?',
                [$userId, 0, ',,,,,,', 0, 0, env('GAME_REG_GOLD'), env('GAME_REG_EXP'), env('GAME_REG_LEVEL'), 0, env('GAME_REG_MONEY'), ',,,,,,', 0, 0, $username, $password, $sex, 1111111111, '', $email, '', '', '']);
            if($sex)
                DB::connection('ddtank')->update('exec SP_Users_RegisterNotValidate @UserName=N\'' . $username . '\',@PassWord=N\'\',@NickName=N\''.$nickname.'\',@BArmID=7008,@BHairID=3158,@BFaceID=6103,@BClothID=5160,@BHatID=1142,@GArmID=7008,@GHairID=3158,@GFaceID=6103,@GClothID=5160,@GHatID=1142,@ArmColor=N\'\',@HairColor=N\'\',@FaceColor=N\'\',@ClothColor=N\'\',@HatColor=N\'\',@Sex=\''.$sex.'\',@StyleDate=0');
            else
                DB::connection('ddtank')->update('exec SP_Users_RegisterNotValidate @UserName=N\'' . $username . '\',@PassWord=N\'\',@NickName=N\''.$nickname.'\',@BArmID=7008,@BHairID=3244,@BFaceID=6204,@BClothID=5276,@BHatID=1214,@GArmID=7008,@GHairID=3244,@GFaceID=6202,@GClothID=5276,@GHatID=1214,@ArmColor=N\'\',@HairColor=N\'\',@FaceColor=N\'\',@ClothColor=N\'\',@HatColor=N\'\',@Sex=\''.$sex.'\',@StyleDate=0');

            DB::connection('ddtank')->update('exec SP_Users_LoginWeb @UserName=N\'' . $username . '\',@Password=N\'\',@FirstValidate=0,@NickName=N\''.$nickname.'\'');

            //return $userId;
            return redirect(route('game'));
        }

        return back()->withErrors(['character.failed']);
    }
}
