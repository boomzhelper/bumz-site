<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostRequest;
use App\Models\News\Post;
use Cache;
use Illuminate\Http\Request;
use SEO;

class PostController extends Controller
{
    public function index(Request $request)
    {
        SEO::setTitle('Новости');
        $page = $request->input('page','1');
        $posts = Cache::remember("posts-{$page}", 10, function () {
            return Post::paginate(5);
        });
        return view('posts.view', ['posts' => $posts]);
    }

    public function expand($id, $path)
    {
        $post = Cache::remember("post-{$id}", 60, function () use ($id) {
            return Post::whereId($id)->first();
        });

        if($post == null)
            return back()->withErrors(['the news was not found']);

        SEO::setTitle($post->title);
        SEO::opengraph()->setUrl(config('app.url'));

        return view('posts.post', compact('post'));
    }

    public function create(CreatePostRequest $request)
    {
        $path = $request->file('image')->store('posts', 'public');
        $postData = $request->validated();
        $postData['image'] = $path;

        Post::create($postData);

        return back()->with('success', [__('post.created')]);
    }
}
