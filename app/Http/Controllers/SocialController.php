<?php

namespace App\Http\Controllers;

use App\Services\SocialService;
use Auth;
use Illuminate\Http\Request;
use Socialite;

class SocialController extends Controller
{
    private $allowed = [
        'facebook',
        'steam',
        'vkontakte',
        'google',
        'yandex'
    ];

    public function redirect($driver)
    {
        if(!in_array($driver, $this->allowed, true)) {
            return redirect()->to('/');
        }

        return Socialite::with($driver)->redirect();
    }

    public function callback(SocialService $service, Request $request, $driver)
    {
        if ($request->has('code') || $request->has('openid_sig')) {
            $user = $service->assignToUser(Socialite::driver($driver)->stateless()->user(), $driver);
            Auth::login($user, true);
            //var_dump(Auth::user());
        }
        return redirect()->to('/');
    }
}
