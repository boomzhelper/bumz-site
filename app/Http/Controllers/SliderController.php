<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSlideRequest;
use App\Models\Slide;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    public function load()
    {
        return [ 'code' => 200, 'result' => Slide::orderBy('id', 'desc')->get() ];
    }

    public function create(StoreSlideRequest $request)
    {
        $path = $request->file('slide')->store('slides', 'public');

        $slide = new Slide();

        $slide->alt = $request->name;
        $slide->link = $request->link;
        $slide->url = $path;

        $slide->save();

        return redirect()->back()->with('success', [__('admin.slide.success')]);
    }
}
