<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\News\Action;
use Cache;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function view()
    {
        $events = Cache::remember('events', 60, function () {
            return Event::with('actions')->orderBy('priority', 'desc')->get();
        });
        //var_dump($events->first()->actions);
        return view('events.view', compact("events"));
    }


    public function show()
    {

    }


}
