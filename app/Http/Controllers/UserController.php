<?php

namespace App\Http\Controllers;

use App\Exceptions\EmailNotUniqueException;
use App\Http\Requests\CreateRequest;
use App\Services\SingleActions\CreateUser;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use SEO;

class UserController extends Controller
{
    use AuthenticatesUsers;

    public function store(CreateRequest $request, CreateUser $createUser)
    {
        try {
            $user = $createUser($request->all());
        } catch (EmailNotUniqueException $e) {
            return back()->withErrors(['user.critical-error']);
        }

        return redirect(route('home'))->with('success', ['user.created']);
    }

    public function create()
    {
        SEO::setTitle('Регистрация');
        SEO::opengraph()->setUrl(config('app.url'));

        return view('registration');
    }
}
