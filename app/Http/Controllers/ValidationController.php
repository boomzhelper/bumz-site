<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;

class ValidationController extends Controller
{
    public function handle(Request $request)
    {
        if($request->has('username')) {
            $validator = Validator::make($request->all(), [
                'username' => ['required', 'string', 'max:20', 'unique:users,name'],
            ]);



            if ($validator->passes()) {
                return response()->json(['success'=>'input received validly']);
            }

            return response()->json(['error'=>$validator->errors()->all()], 422);
        }

        if($request->has('email')) {
            $validator = Validator::make($request->all(), [
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            ]);



            if ($validator->passes()) {
                return response()->json(['success'=>'input received validly']);
            }

            return response()->json(['error'=>$validator->errors()->all()], 422);
        }

        return response()->json(['error'=>'no data'], 404);
    }
}
