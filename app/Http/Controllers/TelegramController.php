<?php

namespace App\Http\Controllers;

use App\Models\TelegramChannelMessage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Telegram;

class TelegramController extends Controller
{
    public function handle()
    {
        /** @var Telegram\Bot\Objects\Update|Telegram\Bot\Objects\Update[] $update */
        $update = Telegram::commandsHandler(true);

        $message = $update->getMessage();

        if($message->chat->type === 'channel' && $message->chat->id == config('telegram.bots.mybot.channelId')) {

            if($message->text === null || $message->text === '') {
                return response('ok');
            }

            $model = new TelegramChannelMessage();

            $model->content = $message->text;
            $model->posted_at = Carbon::now();

            $model->save();
        }

        return response('ok');
    }
}
