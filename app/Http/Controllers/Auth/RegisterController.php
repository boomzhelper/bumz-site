<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\CompleteRegistrationRequest;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Services\SingleActions\UpdateUser;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use SEO;
use TimeHunter\LaravelGoogleReCaptchaV3\Validations\GoogleReCaptchaV3ValidationRule;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => ['required', 'string', 'max:255', 'unique:users,name'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'pwd' => ['required', 'string', 'min:6', 'confirmed'],
            'g-recaptcha-response' => [new GoogleReCaptchaV3ValidationRule('registration')]
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['pwd']),
        ]);

        $role = config('roles.models.role')::where('name', '=', 'Новичок')->first();  //choose the default role upon user creation.
        $user->attachRole($role);

        return $user;
    }

    public function showRegistrationForm()
    {
        SEO::setTitle('Регистрация');
        SEO::opengraph()->setUrl(config('app.url'));


        return view('registration');
    }

    public function showRegistrationCompleteForm()
    {
        SEO::setTitle('Завершение регистрации');
        SEO::opengraph()->setUrl(config('app.url'));


        return view('registration-completing');
    }

    public function completingRegistration(CompleteRegistrationRequest $request, UpdateUser $updateUser)
    {
        $updateUser($request->all());

        return response()->redirectTo(route('home'))->with('success', ['user.register_completed']);
    }
}
