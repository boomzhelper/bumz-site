<?php

namespace App\Http\Controllers;

use App\Models\UserProvider;
use App\Services\SettingsService;
use App\Services\SingleActions\AuthorizeUser;
use Auth;
use DB;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class GameController extends Controller
{
    private $key;
    private $endpoint;
    private $gameServer;
    private $loginServer;
    /**
     * @var string
     */
    private $configPath;

    public function __construct()
    {
        //$this->middleware('auth');
        $this->key = config('game.login_key');
        $this->endpoint = config('game.request');
        $this->gameServer = config('game.server_name');
        $this->loginServer = config('game.login_server_name');
        $this->configPath = config('game.config_path');
    }

    public function createGameViaVkontakte(Request $request, SettingsService $settings, AuthorizeUser $authorizeUser)
    {
        //return $request->all();
        $authKey = $request->input('auth_key');
        $apiId = $request->input('api_id');
        $viewerId = $request->input('viewer_id');
        $appSecret = config("vkontakte.app_secret");

        $key = md5("{$apiId}_{$viewerId}_{$appSecret}");

        if($key == $authKey) {
            $account = UserProvider::whereProvider("vkontakte")
                ->whereProviderId($viewerId)
                ->first();

            if($account == null)
                return view('social-app.login');

            $user = $account->user;

            $data = $authorizeUser($user);

            if($data[0] == 200 && $data[1] == "0")
                return view('social-app.game', ['name'=> $user->name, 'pass' => $data[2], 'config' => $this->configPath]);

            return "YEEES" . $viewerId;
        }
        return redirect()->to(route('home'));
    }

    public function createGame(Request $request, SettingsService $settings, AuthorizeUser $authorizeUser)
    {
        if(!Auth::check())
            return response()->redirectToRoute('home')->withErrors(__('game.must-be-logged-in'));

        $username = Auth::user()->name;

        if(Auth::user()->canLoginCustomUsername())
            if($request->has('login'))
                $username = $request->input('login');

        if($settings->IsMaintenance())
            return back()->withErrors("На сервере проводятся технические работы.");

        $data = $authorizeUser(Auth::user());

        if($data instanceof RedirectResponse)
            return $data;

        $statusCode = $data[0];
        $content = $data[1];

        if($statusCode == 200 && $content == "0")
            return view('game', ['name'=> $username, 'pass' => $data[2], 'config' => $this->configPath]);

        return back()->withErrors($this->getDescription($content));
        //return response()->json(['http-code' => $statusCode, 'status-code' => $content, "status-desc" => "", 'data' => $data, 'unMd5' => $data[0] . $data[1] . $data[4] . $this->key]);
    }

    /**
     * @param string $content
     * @return string
     */
    private function getDescription(string $content): string
    {
        $description = null;
        switch ($content) {
            case "5":
                $description = 'game.login.sign-invalid';
                break;
            case "1":
            case "2":
                $description = 'game.login.data-not-enough';
                break;
            case "4":
                $description = 'game.login.exception';
                break;
            case "3":
                $description = 'game.login.ip-address-not-allowed';
                break;
            case "0":
                $description = 'game.login.ok';
                break;
        }
        return __($description);
    }
}
