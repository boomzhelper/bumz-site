<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\User;
use App\Services\SettingsService;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use SEO;
use Validator;

class PaymentController extends Controller
{
    private $outSecret;
    private $inSecret;
    private $merchantId;

    /**
     * PaymentController constructor.
     */
    public function __construct()
    {
        $this->merchantId = config('payment.merchant_id');
        $this->outSecret = config('payment.secret_out');
        $this->inSecret = config('payment.secret_in');
    }

    public function show()
    {
        SEO::setTitle("Пополнение и перевод");
        SEO::opengraph()->setUrl(config('app.url'));
        return view('payment');
    }

    public function transitToGame(Request $request)
    {
        if(!Auth::check())
            return back()->withErrors('Необходимо авторизироваться прежде чем пополнить счет');

        $validator = Validator::make($request->all(), [
            'sum' => ['required', 'numeric']
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $account = Auth::user();
        $user = DB::connection('ddtank')->table('Sys_Users_Detail')->where('UserName', $account->name)->select('UserID', 'NickName', 'State')->get()->first();

        if($user->State == 1)
            return back()->withErrors("Необходимо выйти с игры прежде чем переводить валюту!");

        if($account->points < $request->input('sum'))
            return back()->withErrors("Сумма привышает то что у вас есть!");

        if($request->input('sum') < 1)
            return back()->withErrors("Сумма не может быть меньше единицы!");

        $account->points -= $request->input('sum');
        $account->save();

        $count = round($request->input('sum') * 40);
        $amount = floor($count / 2000);
        $total = 0;

        $transaction = new Transaction;
        $transaction->type = 1;
        $transaction->amount = $count;
        $transaction->completed_at = Carbon::now();

        $account->transactions()->save($transaction);

        if($amount > 0)
        {
            $happyRecharge = DB::connection('ddtank')->table('Sys_Users_HappyRecharge')->where('UserID', $user->UserID)->select('LotteryCount')->get()->first();
            $total = $happyRecharge->LotteryCount + $amount;
            DB::connection('ddtank')->table('Sys_Users_HappyRecharge')->where('UserID', $user->UserID)->update(['LotteryCount' => $total]);
        }



        $id = DB::connection('ddtank')->table('Sys_Users_Goods')->insertGetId(
            [
                'UserID' => 0,
                'BagType' => 0,
                'TemplateID' => -300,
                'Place' => -1,
                'Count' => $count === 0 ? 1 : $count,
                'Color' => "",
                'StrengthenLevel' => 0,
                'AttackCompose' => 0,
                'DefendCompose' => 0,
                'LuckCompose' => 0,
                'AgilityCompose' => 0,
                'IsBinds' => true,
                'IsUsed' => false,
                'BeginDate' => Carbon::now(),
                'ValidDate' => 0,
                'IsGold' => false,
                'goldValidDate' => 30,
                'latentEnergyCurStr' => "0,0,0,0",
                'latentEnergyNewStr' => "0,0,0,0",
                'latentEnergyEndTime' => Carbon::now(),
            ]
        );

        DB::connection('ddtank')->table('User_Messages')->insert(
            [
                'SenderID' => 0,
                'Sender' => env('IM_SENDER', "Administration"),
                'ReceiverID' => $user->UserID,
                'Receiver' => $user->NickName,
                'Title' => "Перевод внутриигровой валюты",
                'Content' => "В игру было переведено $count аметистов! А так-же у вас есть $total попыток в алмазном колесе!",
                'Type' => 52,
                'Remark' => "Gold:0,Money:0,Annex1:$id,Annex2:,Annex3:,Annex4:,Annex5:,GiftToken:0",
                'Annex1' => "$id",
                'Annex2' => "",
                'Annex3' => "",
                'Annex4' => "",
                'Annex5' => "",
            ]
        );

        return back()->with('success', ['Денюжка переведа в игру! Перезайдите в игру для отображения письма :)']);
    }

    public function test() {
        return DB::table('settings')->where('id', 2)->first()->value * 1324;
    }

    public function receiveNotification(Request $request, SettingsService $settingsService)
    {
        $validator = Validator::make($request->all(), [
            'AMOUNT' => ['required', 'numeric'],
            'MERCHANT_ORDER_ID' => ['required', 'numeric'],
            'SIGN' => ['required', 'string']
        ]);

        if ($validator->fails()) {
            return "NO";
        }

        $m = $this->merchantId;
        $oa = $request->input('AMOUNT');
        $o = $request->input('MERCHANT_ORDER_ID');
        $s = md5("{$m}:{$oa}:{$this->inSecret}:{$o}");

        if ($s != $request->input('SIGN')) {
            return 'wrong sign';
        }

        $bonus = $settingsService->PaymentBonus();

        $user = User::whereId($o)->first();
        $user->points += ($oa * $bonus);
        $user->save();

        $transaction = new Transaction;
        $transaction->type = 0;
        $transaction->amount = $oa;
        $transaction->completed_at = Carbon::now();

        $user->transactions()->save($transaction);

        return "YES";
    }

    public function redirectToPay(Request $request)
    {
        if(!Auth::check())
            return back()->withErrors('Необходимо авторизироваться прежде чем пополнить счет');


        $validator = Validator::make($request->all(), [
            'sum' => ['required', 'numeric', 'min:1', 'max:15000']
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $m = $this->merchantId;
        $oa = $request->input('sum');
        $o = Auth::user()->id;
        $s = md5("{$m}:{$oa}:{$this->outSecret}:{$o}");

        //return Redirect::to(, 304);
        return redirect("https://www.free-kassa.ru/merchant/cash.php?".http_build_query(compact('m', 'oa', 'o', 's')));
    }
}
