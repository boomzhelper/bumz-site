<?php

namespace App\Http\Controllers;

use App\Models\News\Post;
use Cache;
use Illuminate\Http\Request;
use SEO;

class IndexController extends Controller
{
    public function index()
    {
        SEO::setTitle('Главная');
        SEO::opengraph()->setUrl(config('app.url'));
        //SEO::setCanonical('https://codecasts.com.br/lesson');
        //SEO::opengraph()->addProperty('type', 'articles');
        //SEO::twitter()->setSite('@LuizVinicius73');

        $posts = Cache::remember('posts-ordered', 30, function () {
            return Post::orderBy('created_at', 'desc')->take(3)->get();
        });

        return view('index', ['posts' => $posts]);
    }

    public function documents()
    {
        SEO::setTitle('Документы');
        SEO::opengraph()->setUrl(config('app.url'));
        //SEO::setCanonical('https://codecasts.com.br/lesson');
        //SEO::opengraph()->addProperty('type', 'articles');
        //SEO::twitter()->setSite('@LuizVinicius73');

        return view('documents');
    }

    public function policy()
    {
        SEO::setTitle('Политика конфиденциальности');
        SEO::opengraph()->setUrl(config('app.url'));
        //SEO::setCanonical('https://codecasts.com.br/lesson');
        //SEO::opengraph()->addProperty('type', 'articles');
        //SEO::twitter()->setSite('@LuizVinicius73');

        return view('docs.policy');
    }

    public function cookie()
    {
        SEO::setTitle('Cookie Policy');
        SEO::opengraph()->setUrl(config('app.url'));
        //SEO::setCanonical('https://codecasts.com.br/lesson');
        //SEO::opengraph()->addProperty('type', 'articles');
        //SEO::twitter()->setSite('@LuizVinicius73');

        return view('docs.cookie');
    }

    public function rules()
    {
        SEO::setTitle('Правила сервера');
        SEO::opengraph()->setUrl(config('app.url'));
        //SEO::setCanonical('https://codecasts.com.br/lesson');
        //SEO::opengraph()->addProperty('type', 'articles');
        //SEO::twitter()->setSite('@LuizVinicius73');

        return view('docs.rules');
    }
}
