<?php

namespace App\Http\Controllers;

use Auth;
use foo\bar;
use Hash;
use Illuminate\Http\Request;
use SEO;

class SettingsController extends Controller
{
    public function show()
    {
        SEO::setTitle('Настройки');
        SEO::opengraph()->setUrl(config('app.url'));
        return view('settings');
    }

    public function save(Request $request)
    {
        $user = Auth::user();
        $msg = [];

        if(!$request->has('old_password'))
            return back()->withErrors(['settings.old_password.required']);

        if(!Hash::check($request->input('old_password'), $user->password))
            return back()->withErrors(['settings.old_password.do_not_match']);

        if($request->has('password') && $request->input('password') != "") {
            $request->validate([
                'password' => ['required', 'string', 'min:6', 'confirmed'],
            ]);

            $user->password = bcrypt($request->input('password'));
            $user->save();

            $msg[] = __('settings.password.changed');
        }
        if($request->has('email') && $request->input('email') != "") {
            $request->validate([
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            ]);

            $user->markEmailAsUnVerified();

            $user->email = $request->input('email');
            $user->save();

            $user->sendEmailVerificationNotification();

            $msg[] = __('settings.email.changed');
        }

        if(!empty($msg))
            return response()->redirectTo('')->with('success', $msg);

        return back()->withErrors([__('settings.nothing_to_change')]);
    }
}
