<?php

namespace App\Services;

use Auth;
use Cache;
use DB;

class SettingsService
{
    public function IsMaintenance()
    {
        $maintenance = Cache::remember('setting-is-maintenance', 60, static function () {
            return DB::table('settings')->find(1);
        });
        $result = $maintenance !== null ? $maintenance->value === 'true' : true;

        if(Auth::user()->level() > 1) {
            return false;
        }

        return $result;
    }

    public function PaymentBonus()
    {
        $bonus = Cache::remember('setting-payment-bonus', 60, static function () {
            return DB::table('settings')->find(2);
        });

        return $bonus->value;
    }

    public function Pay()
    {
        $bonus = Cache::remember('setting-payment-roulette', 60, function () {
            return DB::table('settings')->find(3);
        });

        return $bonus;
    }
}
