<?php
namespace App\Services\SingleActions;

use App\Exceptions\EmailNotUniqueException;
use App\Exceptions\UserNotLoggedException;
use App\Models\User;
use Auth;

class UpdateUser
{
    /** @var User|null */
    private $user;

    /**
     * UpdateUser constructor.
     * @throws UserNotLoggedException
     */
    public function __construct()
    {
        if(Auth::guest()) {
            throw new UserNotLoggedException('User must be logged before updating');
        }
        $this->user = Auth::user();
    }

    /**
     * @param array $data
     * @return User
     */
    public function __invoke(array $data): User
    {
        if(!empty($data['email']) && $this->user->email !== $data['email']) {
            $this->user->markEmailAsUnVerified();
            $this->user->email = $data['email'];
            $this->user->sendEmailVerificationNotification();
        }

        if(!empty($data['password']))
            $this->user->password = $data['password'];

        $this->user->save();

        return $this->user;
    }
}
