<?php

namespace App\Services\SingleActions;

use App\Exceptions\EmailNotUniqueException;
use App\Models\User;
use Auth;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Str;

class AuthorizeUser
{
    /**
     * @var \Illuminate\Config\Repository
     */
    private $key;
    /**
     * @var \Illuminate\Config\Repository
     */
    private $endpoint;
    /**
     * @var \Illuminate\Config\Repository
     */
    private $loginServer;
    /**
     * @var \Illuminate\Config\Repository
     */
    private $gameServer;

    public function __construct()
    {
        //$this->middleware('auth');
        $this->key = config('game.login_key');
        $this->endpoint = config('game.request');
        $this->gameServer = config('game.server_name');
        $this->loginServer = config('game.login_server_name');
    }

    public function __invoke(User $user)
    {
        $client = new \GuzzleHttp\Client();

        $key = Str::uuid();

        $data = [
            $user->name,
            $key,
            $this->gameServer,
            $this->loginServer,
            time()
        ];

        $data[] = md5($data[0] . $data[1] . $data[4] . $this->key);

        try {
            $curl = $client->request('GET', $this->endpoint . 'CreateLogin.aspx', ['query' => [
                'site' => '',
                'content' => implode("|", $data)
            ]]);
        } catch (GuzzleException $e) {
            return back()->withErrors('game.login.request-return-error');
        }

        $result[] = $curl->getStatusCode();
        $result[] = $curl->getBody()->getContents();
        $result[] = $key;

        return $result;
    }
}
