<?php
namespace App\Services\SingleActions;

use App\Exceptions\EmailNotUniqueException;
use App\Models\User;

class CreateUser
{
    /**
     * @param array $data
     * @return User
     * @throws EmailNotUniqueException
     */
    public function __invoke(array $data): User
    {
        $email = $data['email'];

        if (User::whereEmail($email)->first()) {
            throw new EmailNotUniqueException("$email should be unique!");
        }

        $user = new User;
        $user->name = $data['username'];
        $user->email = $data['email'];
        $user->password = $data['password'];
        $user->save();

        $role = config('roles.models.role')::whereLevel(1)->first();
        $user->attachRole($role);

        return $user;
    }
}
