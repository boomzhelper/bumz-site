<?php /** @noinspection UnNecessaryDoubleQuotesInspection */

namespace App\Services;

use App\Models\User;
use App\Models\UserProvider;
use Laravel\Socialite\Contracts\User as ProviderUser;

class SocialService
{
    /**
     * @param $providerUser ProviderUser
     * @param $driver string
     * @return User
     */
    public function assignToUser($providerUser, $driver)
    {
        $user = null;

        $account = UserProvider::whereProvider($driver)
            ->whereProviderId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            $account = new UserProvider([
                'provider_id' => $providerUser->getId(),
                'provider' => $driver,
                'image' => $providerUser->getAvatar()
            ]);

            if(!is_null($providerUser->getEmail()))
                $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {

                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $this->GenerateName($driver, $providerUser->getId()),
                    'registrationCompleted' => !is_null($providerUser->getEmail())
                ]);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;

        }
    }

    /**
     * @param string $provider
     * @param string $id
     * @return string
     */
    private function GenerateName(string $provider, string $id) : string
    {
        switch ($provider)
        {
            case 'vkontakte':
                $name = "v-";
                break;
            case "facebook":
                $name = "f-";
                break;
            case "steam":
                $name = "s-";
                break;
            case "google":
                $name = "g-";
                break;
            case "yandex":
                $name = "y-";
                break;
            default:
                $name = "u-";
                break;
        }

        $name .= $this->uniqidReal(15);

        return $name;
    }

    function uniqidReal($lenght = 13) {
        // uniqid gives 13 chars, but you could adjust it to your needs.
        if (function_exists("random_bytes")) {
            $bytes = random_bytes(ceil($lenght / 2));
        } elseif (function_exists("openssl_random_pseudo_bytes")) {
            $bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
        } else {
            throw new Exception("no cryptographically secure random function available");
        }
        return substr(bin2hex($bytes), 0, $lenght);
    }
}
