<?php

namespace App\Telegram\Commands;

use Telegram;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;

class StartCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "start";

    protected $aliases = [];

    /**
     * @var string Command Description
     */
    protected $description = "Start Command to get you started";

    /**
     * @inheritdoc
     */
    public function handle()
    {
        if($this->update->getChat()->type == 'group' || $this->update->getChat()->type == 'supergroup')
            return;
        // This will update the chat status to typing...
        $this->replyWithChatAction(['action' => Actions::TYPING]);

        $inlineKeyboard = [
            [
                ['text' => '🚀 Сайт игры', 'url' => config('telegram.bots.mybot.links.site')],
                ['text' => '⚡ Донат', 'url' => config('telegram.bots.mybot.links.shop')],
                ['text' => '💬 Чат', 'url' => config('telegram.bots.mybot.links.chat')]
            ],
            [
                ['text' => '🌎 Новостной канал', 'url' => config('telegram.bots.mybot.links.channel')],
                ['text' => '‼ Группа ВКонтакте', 'url' => config('telegram.bots.mybot.links.vk')]
            ],
            [
                ['text' => '🦊 Администратор сервера', 'url' => config('telegram.bots.mybot.links.admin')]
            ],
        ];

        $reply_inline_markup =  Keyboard::inlineButton([
            'inline_keyboard' => $inlineKeyboard
        ]);
        // This will send a message using `sendMessage` method behind the scenes to
        // the user/chat id who triggered this command.
        // `replyWith<Message|Photo|Audio|Video|Voice|Document|Sticker|Location|ChatAction>()` all the available methods are dynamically
        // handled when you replace `send<Method>` with `replyWith` and use the same parameters - except chat_id does NOT need to be included in the array.
        $this->replyWithMessage(['text' => "Привет мой друг 🌚\r\nС помощью данного бота ты всегда будешь иметь актуальные ссылки на игру и группу ВКонтакте! Пользуйся на здоровье 🤘", 'reply_markup' => $reply_inline_markup]);
    }
}
