<?php

namespace App\Rules;

use App\Models\User;
use Auth;
use Cache;
use Illuminate\Contracts\Validation\Rule;

class UsedToken implements Rule
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|static[]
     */
    private $user;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        if(session('2fa:user:id')) {
            $this->user = User::findOrFail(
                session('2fa:user:id')
            );
        } else {
            $this->user = Auth::user();
        }
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $key = $this->user->id . ':' . $value;

        return !Cache::has($key);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('tfa.used');
    }
}
