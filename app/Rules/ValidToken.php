<?php

namespace App\Rules;

use App\Models\User;
use Auth;
use Crypt;
use Google2FA;
use Illuminate\Contracts\Validation\Rule;

class ValidToken implements Rule
{
    /**
     * @var \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|static[]
     */
    private $user;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        if(session('2fa:user:id')) {
            $this->user = User::findOrFail(
                session('2fa:user:id')
            );
        } else {
            $this->user = Auth::user();
        }
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     * @throws \PragmaRX\Google2FA\Exceptions\IncompatibleWithGoogleAuthenticatorException
     * @throws \PragmaRX\Google2FA\Exceptions\InvalidCharactersException
     * @throws \PragmaRX\Google2FA\Exceptions\SecretKeyTooShortException
     */
    public function passes($attribute, $value)
    {
        $secret = session('secretKey', $this->user->google2fa_secret);

        return Google2FA::verifyKey($secret, $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('tfa.invalid');
    }
}
