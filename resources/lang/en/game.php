<?php

return [
    'must-be-logged-in' => 'You must logged in before entering the game.',
    'login' => [
        'sign-invalid' => "Sign is invalid",
        'data-not-enough' => "Data not enough",
        'exception' => "Server side error",
        'ip-address-not-allowed' => "That IP-address is't allowed here",
        'ok' => "All is fun!",
        'request-return-error' => 'Request was failed'
    ],

];
