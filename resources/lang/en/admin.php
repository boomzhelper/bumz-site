<?php

return [

    'posts' => 'Posts',
    'post' => [
        'add' => 'Create post',
        'edit' => 'Edit post',
    ],

    'slides' => 'Slides',
    'slide' => [
        'add' => 'Create post',
        'edit' => 'Edit post',
    ],

    'telegram' => [
        'title' => 'Telegram',
        'activate' => 'Activate webhook',
    ],


];
