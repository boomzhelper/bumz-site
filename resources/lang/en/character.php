<?php

return [

    'failed' => 'Some error has happened',
    'sex' => [
        'required' => "You must select a gender",
    ],
    'nickname' => [
        'required' => "You must select a character name",
        'exist' => "This nickname already exists",
    ],

];
