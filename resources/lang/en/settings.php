<?php

return [

    'old_password' => [
        'required' => 'Enter the current password',
        'do_not_match' => 'The old password doesn\\\'t match',
    ],

    'nothing_to_change' => 'Nothing to change',

    'email' => [
        'changed' => 'Email address was changed'
    ],
    'password' => [
        'changed' => 'Password was changed'
    ],

];
