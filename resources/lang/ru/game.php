<?php

return [
    'must-be-logged-in' => 'Необходимо авторизоваться прежде чем войти в игру',
    'login' => [
        'sign-invalid' => "Подпись неверна",
        'data-not-enough' => "Нехватает коекаких данных",
        'exception' => "Server side error",
        'ip-address-not-allowed' => "That IP-address is't allowed here",
        'ok' => "All is fun!",
        'request-return-error' => 'Запрос не прошел'
    ],

];
