<?php

return [

    'old_password' => [
        'required' => 'Введите текущий пароль',
        'do_not_match' => 'Старый пароль не совпадает',
    ],

    'nothing_to_change' => 'Нечего менять',

    'email' => [
        'changed' => 'Адрес электронной почты был изменен'
    ],
    'password' => [
        'changed' => 'Пароль был изменен'
    ],

];
