(function($) {

    const methods = {
        init: function(options) {
            return this.each(function(){
                let $this = $(this),
                    data = $this.data('slider');
                // Если плагин ещё не проинициализирован
                if (!data) {

                    let settings = $.extend( {
                        'current': 1,
                        'count': $(this).find('.slide-wrapper').children().length,
                        'interval': 10000,
                        'dynamic': $this.data('dynamic'),
                        'api': $this.data('api')
                    }, options);
                    //alert(typeof(settings.dynamic));
                    $(this).data('slider', settings);
                    if(!settings.dynamic)
                        methods['render'].apply($this);
                    else {
                        $.getJSON(settings.api+'/slides', null, function (json) {
                            if(json.code === 200)
                                methods['render'].call($this, json.result);
                        });
                    }

                    $(this).find('.next').click(function() {
                        methods['next'].apply($this);
                    });

                    $(this).find('.prev').click(function() {
                        methods['previous'].apply($this);
                    });
                    let switchInterval = undefined;
                    let switchSlide = function() {
                        methods['next'].apply($this)
                    };
                    $(this).find('.viewport').hover(function() {
                        clearInterval(switchInterval);
                    }, function() {
                        switchInterval = setInterval(switchSlide, settings.interval);
                    });


                }
            });
        },
        destroy: function() {

            return this.each(function(){

                let $this = $(this),
                    data = $this.data('slider');

                // пространства имён рулят!!11
                //$(window).unbind('.tooltip');
                //data.tooltip.remove();
                $this.removeData('slider');

            })

        },
        render: function(jsonData=null) {

            let data = $(this).data('slider'),
                wrapper = $(this).find('.slide-wrapper'),
                navigation = $(this).find('.navigation-buttons');

            if(data) {
                let isDynamic = jsonData != null;
                let count = !isDynamic ? wrapper.children().length : jsonData.length;

                for(let i = 0; i < count; i++) {
                    $('<li>').addClass('to-slide').appendTo(navigation);
                }

                if(isDynamic) {
                    data.count = count;
                    $(this).data('slider', data);
                    $.each(jsonData, function () {
                        let image = $(`<img src="${window.location.origin + "/storage/" + this.url}" alt="${this.alt}" class="slide-img">`);
                        let a = $(`<a href="${this.link}">`);
                        let li = $('<li>').addClass('slide');
                        image.appendTo(a);
                        a.appendTo(li);
                        li.appendTo(wrapper);
                    });
                }
                $(navigation.children()[data.current-1]).addClass('selected');
            }
        },
        next: function() {
            let data = $(this).data('slider'),
                wrapper = $(this).find('.slide-wrapper'),
                viewport = $(this).find('.viewport'),
                navigation = $(this).find('.navigation-buttons');

            if(data) {
                if (data.current === data.count || data.current <= 0 || data.current > data.count) {
                    wrapper.css('transform', 'translate(0, 0)');
                    $(navigation.children()[data.current-1]).removeClass('selected');
                    data.current = 1;
                    $(navigation.children()[data.current-1]).addClass('selected');
                } else {
                    let translateWidth = -viewport.width() * (data.current);
                    wrapper.css({
                        'transform': 'translate(' + translateWidth + 'px, 0)',
                        '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
                        '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
                    });
                    $(navigation.children()[data.current-1]).removeClass('selected');
                    $(navigation.children()[data.current]).addClass('selected');
                    data.current++;
                }
            }
        },
        previous: function() {
            let data = $(this).data('slider'),
                wrapper = $(this).find('.slide-wrapper'),
                viewport = $(this).find('.viewport'),
                navigation = $(this).find('.navigation-buttons');

            if(data) {
                if (data.current === 1 || data.current <= 0 || data.current > data.count) {
                    let translateWidth = -viewport.width() * (data.count - 1);
                    wrapper.css({
                        'transform': 'translate(' + translateWidth + 'px, 0)',
                        '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
                        '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
                    });
                    $(navigation.children()[data.current-1]).removeClass('selected');
                    data.current = data.count;
                    $(navigation.children()[data.current-1]).addClass('selected');
                } else {
                    let translateWidth = -viewport.width() * (data.current - 2);
                    wrapper.css({
                        'transform': 'translate(' + translateWidth + 'px, 0)',
                        '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
                        '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
                    });
                    data.current--;
                    $(navigation.children()[data.current]).removeClass('selected');
                    $(navigation.children()[data.current-1]).addClass('selected');
                }
            }
        }
    };

    $.fn.slider = function(method) {
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метод с именем ' +  method + ' не существует для jQuery.slider' );
        }
    };
})(jQuery);
