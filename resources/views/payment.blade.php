@extends('layouts.app')

@push('async-css')
    <link rel="stylesheet" href="{{ mix('/css/payment.css') }}">
@endpush

@section('content')
    <div class="block information">
        <h2>Памятка к пополнению и переводу</h2>
        <p class="notice">
            <span>Необходимо выйти из игры перед тем как переводить деньги! Курс перевода <b>ОП</b> в <b>аметисты</b> составляет <b>1</b> к <b>40</b>.
                 Если по какой-то причине аметисты или ОП не пришли на счет, то <span class="no">необходимо</span> обратиться в
                <a href="https://vk.com/im?media=&sel=-187884224"><b><em>тех. поддержку</em></b></a> <sup>(мб даже ответят)</sup>
             </span>
        </p>
    </div>
    <div class="block payment half-of">
        <h2>Пополнение счета</h2>
        <p class="notice">
            <img class="inline" width="36" height="26" src="https://tbank.mail.ru/resources/iframe/images/connection_ico.png" alt="">
            <span class="inline">
                <span class="head" style="font-size: 13px; font-weight: bold;">Безопасное соединение</span>
                <span class="body" style="font-size: 10px;">Передаваемые данные зашифрованы 256-битным ключом</span>
            </span>
        </p>
        <form id="payment" action="{{route('donate')}}" method="POST">
            @csrf
            <label>
                <input class="amount" type="number" step="0.01" name="sum" value="100.00" required>
            </label>
            <input class="submit" type="submit" value="Перейти к пополнению">
        </form>
    </div><!--
    --><div class="block transit half-of">
        <h2>Перевод в игру</h2>
        <p class="notice">
            <img class="inline" width="36" height="26" src="{{ url('/img/attention_ico.png') }}" alt="">
            <span class="inline">
                <span class="head" style="font-size: 13px; font-weight: bold;">Внимание!</span>
                <span class="body" style="font-size: 10px;">Каждые 2000 аметистов = 1 попытка алмазного колеса</span>
            </span>
        </p>
        <form id="transit" action="{{route('transit')}}" method="POST">
            @csrf
            <label>
                <input id="amount" class="amount" type="number" step="0.01" name="sum" value="100.00" required>
            </label>
            <input id="transitToGame" class="submit" type="submit" value="Перевести в игру">
        </form>
    </div>
    <div class="block screenshots">
        <div class="gallery">
            <img src="{{ url('img/screenshots/shop01.png') }}" alt="arm shop">
        </div>
    </div>
@endsection

@push("async-scripts")
    $("#amount").attr({
        "max" : $("#balance").text(),        // substitute your own
    });
    $("#amount").keyup(function() {
        if(Math.round($('#amount').val() * 100) / 100 > Math.round($("#balance").text() * 100) / 100)
        {
            $('#amount').val(Math.round($("#balance").text() * 100) / 100);
        }
        let amount = Math.round($('#amount').val() * 40);
        $('#transitToGame').val("Перевести в игру " + amount + " аметистов")
    });
    $("#amount").keyup();
@endpush
