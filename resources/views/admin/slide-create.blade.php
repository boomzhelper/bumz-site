@extends('layouts.app')

@push('async-scripts')
    async('{{ mix('js/admin-slide-create.js') }}', function() {
        $('#slideViewer').slidePreviewer();
    });
@endpush

@push('async-css')
    <link rel="stylesheet" href="{{ mix('/css/slide.create.css') }}">
@endpush

@section('content')
    <div class="block">
        <h2>Документы</h2>
        <form action="{{ route('slide.create') }}" method="post" id="slide-create" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="third">
                    <label class="input-name" for="name">Название:</label>
                    <input class="input" type="text" id="name" name="name" required>
                </div>
                <div class="third">
                    <label class="input-name" for="link">Действие:</label>
                    <input class="input" type="text" id="link" name="link" required>
                </div>
                <div class="third" id="parent-previewer">
                    <label class="input-name" for="slide-image">Слайд:</label>
                    <div class="input" id="slideViewer" data-form="#parent-previewer">
                        <input type="file" name="slide" class="uploader" id="uploader" accept="image/jpeg,image/png" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="full">
                    <input type="submit" value="Создать" class="submit-btn">
                </div>
            </div>
        </form>
    </div>
@endsection
