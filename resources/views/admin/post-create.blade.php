@extends('layouts.app')

@push('async-scripts')
    async('{{ mix('js/admin-slide-create.js') }}', function() {
        //$('#slideViewer').slidePreviewer();
    $(document).ready(function () {
    $('#title').on('input', function() {
    let text = $('#title').val();
    $('#path').val(transliterate(text));
    });
    });
    });
@endpush

@push('async-css')
    <link rel="stylesheet" href="{{ mix('/css/post-create.css') }}">
@endpush

@section('content')
    <div class="block">
        <h2 class="h2">Создание поста</h2>
        <form action="{{ route('admin.post-create') }}" method="post" id="slide-create" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="column third">
                    <label class="input-name" for="title">Название:</label>
                    <input class="input" type="text" id="title" name="title" required>
                </div>
                <div class="column third">
                    <label class="input-name" for="path">Путь к посту:</label>
                    <input class="input" type="text" id="path" name="path" required>
                </div>
                <div class="column third" id="parent-previewer">
                    <label class="input-name" for="slide-image">Превьюшка:</label>
                    <div class="input" id="slideViewer" data-form="#parent-previewer">
                        <input type="file" name="image" class="uploader" id="uploader" accept="image/jpeg,image/png" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column full">
                    <label class="input-name" for="short_description">Короткое описание:</label>
                    <textarea name="short_description" id="short_description" cols="30" rows="10" class="post__description"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="column full">
                    <label class="input-name" for="full_description">Текст поста:</label>
                    <textarea name="full_description" id="full_description" cols="30" rows="10" class="post__description"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="column full">
                    <input type="submit" value="Создать" class="button__without-border">
                </div>
            </div>
        </form>
        <script
            src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
            crossorigin="anonymous"></script>
        <script>

            $(document).ready(function() {
                $('#full_description').bind('input propertychange', function() {
                    if(this.value.length){
                        $('#preview').html(this.value)
                    }
                });
            });

            function transliterate(word){
                let answer = ""
                    , a = {};

                a["Ё"]="YO";a["Й"]="I";a["Ц"]="TS";a["У"]="U";a["К"]="K";a["Е"]="E";a["Н"]="N";a["Г"]="G";a["Ш"]="SH";a["Щ"]="SCH";a["З"]="Z";a["Х"]="H";a["Ъ"]="'";
                a["ё"]="yo";a["й"]="i";a["ц"]="ts";a["у"]="u";a["к"]="k";a["е"]="e";a["н"]="n";a["г"]="g";a["ш"]="sh";a["щ"]="sch";a["з"]="z";a["х"]="h";a["ъ"]="'";
                a["Ф"]="F";a["Ы"]="I";a["В"]="V";a["А"]="a";a["П"]="P";a["Р"]="R";a["О"]="O";a["Л"]="L";a["Д"]="D";a["Ж"]="ZH";a["Э"]="E";
                a["ф"]="f";a["ы"]="i";a["в"]="v";a["а"]="a";a["п"]="p";a["р"]="r";a["о"]="o";a["л"]="l";a["д"]="d";a["ж"]="zh";a["э"]="e";
                a["Я"]="Ya";a["Ч"]="CH";a["С"]="S";a["М"]="M";a["И"]="I";a["Т"]="T";a["Ь"]="'";a["Б"]="B";a["Ю"]="YU";
                a["я"]="ya";a["ч"]="ch";a["с"]="s";a["м"]="m";a["и"]="i";a["т"]="t";a["ь"]="'";a["б"]="b";a["ю"]="yu";
                a[" "]="_";
                for (i in word){
                    if (word.hasOwnProperty(i)) {
                        if (a[word[i]] === undefined){
                            answer += word[i];
                        } else {
                            answer += a[word[i]];
                        }
                    }
                }
                return answer;
            }
        </script>
    </div>
    <div class="block" id="preview">

    </div>
@endsection
