@extends('layouts.app')

@push('async-css')
    <link rel="stylesheet" href="{{ mix('/css/news.css') }}">
    <link rel="stylesheet" href="{{ mix('/css/modules/pagination.css') }}">
@endpush

@section('content')
    @Posts(['isPaginate' => true, 'posts' => $posts])
    @endPosts
@endsection
