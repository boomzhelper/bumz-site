@extends('layouts.app')

@push('async-css')
    <link rel="stylesheet" href="{{ mix('/css/news.css') }}">
@endpush

@section('content')
    <article class="post block">
        <h2>{{$post->title}}</h2>
        <div>
            {!! $post->full_description !!}
        </div>
    </article>
@endsection
