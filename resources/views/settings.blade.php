@extends('layouts.app')

@section('title', 'Настройки')

@push('async-css')
    <link rel="stylesheet" href="{{ mix('/css/settings.css') }}">
@endpush

@push('async-scripts')
    async('{{ mix('/js/validator.js') }}', function() {
        $('#account_settings').validator();
    });
@endpush

@section('content')
    <div class="block settings">
        <div>
            <h2>Настройки аккаунта</h2>
            <form id="account_settings" action="{{ route('settings-save') }}" method="post">
                @csrf
                <div class="row">
                    <div class="column full">
                        <div class="form-group">
                            <label for="s_pwd">Текущий пароль *</label>
                            <input id="s_pwd" name="old_password" type="password" required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="column full">
                        <div class="form-group has-feedback">
                            <label for="s_new_pwd">Введите новый пароль</label>
                            <input id="s_new_pwd" name="password" type="password" data-pattern-error="Пароль должен содержать буквы верхнего и нижнего регистра, а так же цифры."  pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$">
                            <div class="tip-message-body">
                                <div class="outer tip-message">
                                    <div class="inner help-block with-errors">
                                        <span class="empty">Введите новый пароль, если вы хотите изменить его.</span>
                                        <span class="success">После смены пароля на ваш e-mail будет высланно уведомление</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="s_new_pwd_confirmation">Повторите новый пароль</label>
                            <input id="s_new_pwd_confirmation" name="password_confirmation" type="password" data-match="#s_new_pwd" data-match-error="Ваш новый пароль и подтверждение пароля не совпадают.">
                            <div class="tip-message-body">
                                <div class="outer tip-message">
                                    <div class="inner help-block with-errors">
                                        <span class="empty">Подвердите новый пароль, если вы собираетесь изменить его.</span>
                                        <span class="success">После смены пароля на ваш e-mail будет высланно уведомление</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="s_email">Текущий E-mail</label>
                            <input id="s_email" name="email" type="email" disabled value="{{ Auth::user()->email ?? "не задан" }}">
                            <div class="tip-message-body">
                                <div class="outer tip-message">
                                    <div class="inner help-block with-errors">
                                        <span class="empty">Это ваш текущий E-mail адресс</span>
                                        <span class="success">Это ваш текущий E-mail адресс</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="s_new_email">Введите новый E-mail</label>
                            <input id="s_new_email" name="email" type="email">
                            <div class="tip-message-body">
                                <div class="outer tip-message">
                                    <div class="inner help-block with-errors">
                                        <span class="empty">Введи новый email адресс. На него будет выслано письмо с подверждением</span>
                                        <span class="success">Вам необходимо будет подтверить новый адрес с обеих почт.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <hr>
                </div>
                <div class="row">
                    <div class="form-group">
                        <input class="save-button" type="submit" value="сохранить изменения">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <p>
                            * Обязательное поле. Если вы забыли пароль, то, пожалуйста, обратитесь в <a href="#">службу поддержки</a> нашего сервиса.
                        </p>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
