@extends('layouts.app')

@push('async-css')
    <link rel="stylesheet" href="{{ mix('css/events.css') }}">
@endpush

@section('content')
    <div>
    @php $current = 0; @endphp
    @foreach($events as $event)
        @if($event->block_size == "full")
            <div class="row">
                <div class="column full">
                    <div class="block event {{ $event->size == App\Models\Event::LARGE ? "large" : "" }}">
                        <div class="fl left margin-right-s10">
                            <img src="{{ $event->image }}" alt="VK">
                        </div>
                        <div>
                            <h3>{{ $event->title }}</h3>
                            <span>{{ $event->short_description }}</span>
                            @if($event->actions)
                                @foreach($event->actions as $action)
                                    @switch($action->type)
                                        @case('href')
                                            <a class="more-btn" href="{{ $action->action }}">Подробнее</a>
                                        @break
                                    @endswitch
                                @endforeach
                            @endif
                        </div>
                        <div class="fl clear"></div>
                    </div>
                </div>
            </div>
        @else

            @if($current == 0)
            <div class="row">
            @endif
                <div class="column s{{ $event->block_size }}">
                    <div class="block event">
                        <div class="fl left margin-right-s10">
                            <img src="{{ $event->image }}" alt="VK">
                        </div>
                        <div>
                            <h3>{{ $event->title }}</h3>
                            <span>{{ $event->short_description }}</span>
                            @if($event->actions)
                                @foreach($event->actions as $action)
                                    @switch($action->type)
                                        @case('href')
                                        <a class="more-link" href="{{ $action->action }}">Подробнее..</a>
                                        @break
                                    @endswitch
                                @endforeach
                            @endif
                        </div>
                        <div class="fl clear"></div>
                    </div>
                </div>
            @if($current == 1 or $loop->last)
            </div>
            @endif
            @php $current == 2 ? $current = 0: $current++; @endphp

        @endif
    @endforeach
    </div>
@endsection
