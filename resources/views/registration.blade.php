@extends('layouts.app')

@section('title', 'Регистрация')

@push('async-css')
    <link rel="stylesheet" href="{{ mix('/css/registration.css') }}">
@endpush

@push('async-scripts')
    async('{{ mix('/js/validator.js') }}', function() {
        $('#registration').validator();
    });
@endpush

@section('content')
    <div class="block registration">
        <h2>Регистрация</h2>
        <p class="notice">
            Для регистрации нужно указать действующий адрес электронной почты. Он будет использоваться для
            восстановления данных аккаунта и общения с администрацией игры. Изменить его позднее вы сможете только
            через службу поддержки. Если у вас нет почты, зарегистрируйте ее на любом почтовом сервисе,
            например Gmail / Yandex / Mail.ru.
        </p>
        <form id="registration" action="{{ route('user.store') }}" method="POST">
            @csrf
            {!!  GoogleReCaptchaV3::renderField('regcaption','registration') !!}
            <div class="row">
                <div class="form-group has-feedback">
                    <input data-remote="/validation/registration" data-pattern-error="Логин может содержать только латинские буквы и цифры" data-error="Этот логин уже занят" data-required-error="Введите желаемый логин" type="text" name="username" placeholder="Логин" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$" required>
                    <div class="tip">
                        <p class="outer">
                            <span class="inner help-block with-errors">
                                <span class="empty">Введите желаемый логин</span>
                                <span class="success">Все верно</span>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group has-feedback">
                    <input data-remote="/validation/registration" data-pattern-error="Email адрес не соотвуствует правилам" data-error="Данный email адрес уже занят" data-required-error="Введите действующий адрес электронной почты" type="email" name="email" placeholder="E-mail" required>
                    <div class="tip">
                        <p class="outer">
                            <span class="inner help-block with-errors">
                                <span class="empty">Введите действующий адрес электронной почты</span>
                                <span class="success">Все верно</span>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group has-feedback">
                    <input type="password" name="password" placeholder="Пароль" id="pwd" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" required>
                    <div class="tip">
                        <p class="outer">
                            <span class="inner help-block with-errors">
                                <span class="empty">Придумайте пароль</span>
                                <span class="success">Все верно</span>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group has-feedback">
                    <input data-match="#pwd" type="password" name="password_confirmation" placeholder="Повторите пароль" required>
                    <div class="tip">
                        <p class="outer">
                            <span class="inner help-block with-errors">
                                <span class="empty">Введите еще раз придуманный пароль</span>
                                <span class="success">Все верно</span>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <hr>
            </div>
            <div class="row">
                <div class="form-group">
                    <p class="user-agreement">Зарегистрировавшись, вы автоматически принимаете
                        <a href="{{ route('rules') }}" target="_blank">пользовательское соглашение</a>
                        и <a href="{{ route('policy') }}">политику конфиденциальности</a> нашего сервиса.</p>
                    <input class="registration-button" type="submit" value="Зарегистрироваться">
                </div>
            </div>
        </form>
    </div>
@endsection
