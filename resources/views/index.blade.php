@extends('layouts.app')

@section('slider')
    <div id="slider" class="block slider" data-dynamic="true" data-api="{{ config('plugins.slider_api') }}">
        <div class="viewport">
            <ul class="slide-wrapper">

            </ul>

            <div class="prev"></div>
            <div class="next"></div>

            <ul class="navigation-buttons">
            </ul>

        </div>
    </div>
@endsection

@push('async-scripts')
    async('{{ mix('/js/slider.js') }}', function() {
        $('#slider').slider();
    });
@endpush

@push('async-css')
    <link rel="stylesheet" href="{{ mix('/css/news.css') }}">
@endpush
@push('css')
    <link rel="stylesheet" href="{{ mix('/css/modules/slider.css') }}">
@endpush

@section('content')
    @Posts(['isPaginate' => false, 'posts' => $posts])
    @endPosts
@endsection
