<div class="block">
    <div class="user-info">
        <img src="{{ url('img/user/avatar.png') }}" alt="User Avatar" class="border">
        <p class="username">{{ Auth::user()->name }}</p>
        <p class="balance">У вас: <span id="balance">{{ Auth::user()->points }}</span> ОП. <sup><small>[?]</small></sup></p>
        <div style="clear: both"></div>
    </div>
    <div class="user-navigation">
        @php var_dump(Auth::user()->level()); @endphp
        @if(!(new App\Services\SettingsService)->IsMaintenance())
        <div class="row">
            <a href="{{ route('game') }}"><button class="full">Играть</button></a>
        </div>
        @else
        <div class="row">
            <span style="font-size: 28px;">Технические работы</span>
        </div>
        @endif
        <div class="row">
            <a href="{{ route('donate') }}"><button class="full">Пополнить счет</button></a>
        </div>
        <div class="row">
            <a href="{{ route('settings') }}" class="link"><button class="">Настройки</button></a>
            <a href="{{ route('logout') }}" class="link"><button class="">Выход</button></a>
        </div>
        <div class="row">
            @if (Auth::user()->google2fa_secret)
                <a href="{{ url('2fa/disable') }}" class=""><button class="full">Отключить 2FA</button></a>
            @else
                <a href="{{ url('2fa/enable') }}" class=""><button class="full">Включить 2FA</button></a>
            @endif
        </div>
    </div>
</div>
