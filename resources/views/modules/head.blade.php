<meta charset="UTF-8">
<meta name="viewport" content="width=960">
<!-- Chrome, Firefox OS and Opera -->
<meta name="theme-color" content="#1a014e">
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#1a014e">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-status-bar-style" content="#1a014e">
{!! SEO::generate(config('app.debug')) !!}
<link rel="stylesheet" href="{{ mix('/css/index.css') }}">
<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
<link rel="shortcut icon" href="{{ url('favicon.ico') }}" type="image/x-icon">
@stack('css')

