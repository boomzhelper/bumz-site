<div class="block auth">
    <h3 class="heading">Авторизация</h3>
    <form method="POST" action="{{ url('user/login') }}">
        @csrf
        <label for="name">Логин:</label>
        <input type="text" id="name" name="name">
        <label for="password">Пароль:</label>
        <input type="password" id="password" name="password">
        <div class="login-form-buttons">
            <button type="submit">Вход</button>
            <button id="GTR">Регистрация</button>
        </div>
        <div class="fl clear"></div>
        <div id="authc"></div>
        @php GoogleReCaptchaV3::render(['authc'=>'auth']); @endphp
    </form>
    <div class="social">
        <div class="social-icons">
            <a href="{{ route('social-auth', 'vkontakte') }}"><span><i class="vk"></i></span></a>
            <a href="{{ route('social-auth', 'facebook') }}"><span><i class="facebook"></i></span></a>
            <a href="{{ route('social-auth', 'steam') }}"><span><i class="steam"></i></span></a>
            <a href="{{ route('social-auth', 'google') }}"><span><i class="google"></i></span></a>
            <a href="{{ route('social-auth', 'yandex') }}"><span><i class="yandex"></i></span></a>
        </div>
    </div>

</div>
@push('async-scripts')
    $('#GTR').click(function(e) {
        e.preventDefault();
        window.location.href = "/registration";
    });
@endpush
