@push('async-scripts')
    async('{{ mix('js/jquery.inputmask.js') }}', function() {
    $("#secret").inputmask({"mask": "999 999", "removeMaskOnSubmit": true});
    });
@endpush

@push('async-css')
    <link rel="stylesheet" href="{{ mix('/css/validateTwoFactor.css') }}">
@endpush

<div class="two-factor-auth">
    @if($enableHead)
    <h2 class="h2">{{ $title }}</h2>
    @endif
    <img src="{{ asset('img/tfa.png') }}" alt="two factor" class="fl right">
    <div class="panel-body">
        <form class="form-horizontal" role="form" method="POST" action="{{ $action }}">
            @csrf
            <label class="col-md-4 control-label" for="secret">Одноразовый пароль</label>
            <input type="text" id="secret" class="form-control" name="totp">
            <input type="submit" value="{{ $slot }}">
        </form>
    </div>
    <div class="fl clear"></div>
</div>
