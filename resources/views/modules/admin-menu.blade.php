<link rel="stylesheet" href="{{ mix('/css/modules/dropdown.css') }}">
<div class="dropdown-menu">
    <nav>
        <ul>
            @if(Auth::user()->hasOnePermission(['post.add', 'post.edit']))
            <li><a href="#" aria-haspopup="true">{{ __('admin.posts') }}</a>
                <ul class="dropdown" aria-label="submenu">
                    @permission('post.add')
                    <li><a href="{{ route('admin.post-create') }}">{{ __('admin.post.add') }}</a></li>
                    @endpermission

                    @permission('post.edit')
                    <li><a href="#">{{ __('admin.post.edit') }}</a></li>
                    @endpermission
                </ul>
            </li>
            @endif

            @if(Auth::user()->hasOnePermission(['slide.add', 'slide.edit']))
                <li><a href="#" aria-haspopup="true">{{ __('admin.slides') }}</a>
                    <ul class="dropdown" aria-label="submenu">
                        @permission('slide.add')
                        <li><a href="{{ route('slide.create') }}">{{ __('admin.slide.add') }}</a></li>
                        @endpermission

                        @permission('slide.edit')
                        <li><a href="#">{{ __('admin.slide.edit') }}</a></li>
                        @endpermission
                    </ul>
                </li>
            @endif

            @if(Auth::user()->hasOnePermission(['telegram.activate']))
                <li><a href="#" aria-haspopup="true">{{ __('admin.telegram.title') }}</a>
                    <ul class="dropdown" aria-label="submenu">
                        @permission('telegram.activate')
                        <li><a href="{{ route('admin.telegram.activate') }}">{{ __('admin.telegram.activate') }}</a></li>
                        @endpermission
                    </ul>
                </li>
            @endif
        </ul>
    </nav>
</div>
