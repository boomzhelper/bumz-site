@forelse ($posts as $post)
    <article class="block news">
        <aside class="ib top">
            <a href="{{ route('post', [$post->id, $post->path]) }}"><img src="{{ asset('storage/'.$post->image) }}" alt="" width="120" height="120"></a>
        </aside>
        <section class="ib top">
            <header>
                <a href="{{ route('post', [$post->id, $post->path]) }}">{{ $post->title }}</a><span>{{ $post->created_at->format('d.m.Y') }}</span>
            </header>
            <p class="news-preview">
                {{ $post->short_description }}
            </p>
            <a href="{{ route('post', [$post->id, $post->path]) }}" class="read-more">
                <div class="button">Подробнее...</div>
            </a>
        </section>
    </article>
@empty
    <article class="block news">
        NO NEWS WAS FOUND
    </article>
@endforelse
@if($isPaginate)
    {{  $posts->render() }}
@endif
