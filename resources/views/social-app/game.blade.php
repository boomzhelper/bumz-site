<html lang="ru">
<head>
    <style>
        .background {
            background-image: url('{{ url("img/bg-all.jpg") }}');
            background-position: center;
            background-repeat: no-repeat;
            box-shadow: 0 0 32px 32px #311c47 inset;
            width: 1920px;
            height: 1200px;
            z-index: -1;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
        body {
            background-color: #311c47;
            margin:0;
            padding:0;
            overflow: hidden;
        }
    </style>
    <title>Игра</title>
</head>
<body>
<div class="background"></div>
<div style="display: flex;
                    align-items: center;
                    justify-content: center;
                    height: 100%;
                    min-width: 1000px;
                    min-height: 600px;
                    ">
    <div style="">
        <object id="7road-ddt-game" name="Main" width="1000" height="600" align="middle">
            <param name="allowScriptAccess" value="always">
            <param name="movie" value="{{ config('game.flash') }}/preloader.swf?user={{ $name }}&flashURL={{ urlencode(config('game.flash')) }}&key={{ $pass }}&config={{ $config }}">
            <param name="quality" value="high">
            <param name="menu" value="false">
            <param name="bgcolor" value="#000000">
            <param name="FlashVars" value="site=bb.mail.ru&sitename=&rid=&enterCode=">
            <param name="allowScriptAccess" value="always">
            <param name="wmode" value="direct">
            <embed flashvars="site=bb.mail.ru&sitename=&rid=&enterCode=" src="{{ config('game.flash') }}/preloader.swf?user={{ $name }}&flashURL={{ urlencode(config('game.flash')) }}&key={{ $pass }}&config={{ $config }}" width="1000" height="600" align="middle" quality="high" name="Main" allowscriptaccess="always" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" wmode="direct">
        </object>
    </div>
</div>
</body>
</html>
