<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Авторизация</title>
        <style>
            @import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap');
            .background {
                background-image: url('{{ url("img/login-background.jpg") }}');
                background-position: center;
                background-repeat: no-repeat;
                box-shadow: 0 0 32px 32px #311c47 inset;
                width: 1920px;
                height: 1200px;
                z-index: -1;
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
            }
            body {
                background-color: #311c47;
                margin:0;
                padding:0;
                overflow: hidden;
                font-family: 'Roboto', sans-serif;
            }

            .authorization {
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                text-align: center;
                width: 100%;
            }

            .login-form, .information, .actions {
                background-color: rgba(0,0,0, 0.7);
                box-sizing: border-box;
                width: 230px;
                padding: 10px;
                color: white;
            }
            .login-form, .information, .actions {
                display: inline-block;
                vertical-align: top;
                height: 164px;
            }
            .information {
                width: 350px;
            }
            .actions {
                width: 584px;
                margin-top: 4px;
                height: auto;
            }
            .row {
                margin-bottom: 5px;
                margin-top: 5px;
            }
            .row:last-child {
                margin-top: 10px;
            }
            input[type=text], input[type=password] {
                width: 100%;
                border: none;
                padding: 5px;
            }
            input[type=submit] {
                width: 100%;
                border: none;
                padding: 5px;
            }
            h2 {
                margin-bottom: 15px;
                margin-top: 5px;
            }
            .switch-btn {
                color: white;
            }
        </style>
    </head>
    <body>
        <div class="background"></div>
        <div class="authorization">
            <div class="login-form">
                <h2>Авторизация</h2>
                <div class="row">
                    <input type="text" name="name" id="name" placeholder="Логин">
                </div>
                <div class="row">
                    <input type="password" name="password" id="password" placeholder="Пароль">
                </div>
                <div class="row">
                    <input type="submit" value="Войти">
                </div>
            </div>
            <div class="information">
                <h2>Как? Что? Зачем?</h2>
                <div style="text-align: left">
                    Вам необходимо авторизироваться или зарегистрироваться на нашем сервере для того чтобы мы могли
                    предоставить доступ к игровому контенту, это не займет у вас много времени. :)
                </div>
            </div>
            <div class="actions">
                <div>

                </div>
            </div>
        </div>
    </body>
</html>
