@extends('layouts.app')

@section('title', 'Обновите страницу')

@section('content')
    <div class="block error-401">
        <h1>Ошибка! Вернитесь назад и попробуйте еще раз!</h1>
        <p>{{ $error }}</p>
    </div>
@endsection
