@extends('layouts.app')

@section('content')
    <div class="block">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <h2 class="h2">Отключение двухфакторной аутентификация</h2>
                <div class="block-body">
                    <span style="font-size: 16px">Для отключение двухфакторной аутентификация, получите 6 значный одноразовый пароль в своем мобильном приложение и введите его <label for="secret">здесь!</label></span>
                    @TwoFactor(['action' => route('disableTwoFactor'), 'enableHead' => false])
                    Отключить
                    @endTwoFactor
                </div>
            </div>
        </div>
    </div>
@endsection
