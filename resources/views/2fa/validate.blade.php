@extends('layouts.app')


@section('content')
    <div class="block">
        <div class="row">
            @TwoFactor(['title' => 'Двухфакторная аутентификация', 'action' => route('LoginVia2fa'), 'enableHead' => true])
            Авторизироваться
            @endTwoFactor
        </div>
    </div>
@endsection
