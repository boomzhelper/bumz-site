@extends('layouts.app')

@push('async-scripts')
    async('{{ mix('js/jquery.inputmask.js') }}', function() {
        $("#secret-code").inputmask({"mask": "999 999", "removeMaskOnSubmit": true});
    });
@endpush

@push('async-css')
    <link rel="stylesheet" href="{{ mix('/css/enableTwoFactor.css') }}">
@endpush

@section('content')
    <div class="block two-factor">
        <div class="row">
            <h2 class="h2">Двухфакторная аутентификация</h2>
            <div class="block-body">
                <img alt="Image of QR barcode" src="{{ $image }}" class="fl left"/>
                <div class="text">
                    1. Откройте ваше мобильное приложение для двухфакторной аутентификации(2FA) и отсканируйте справа QR-код.
                    Если ваше мобильное приложение 2FA не поддерживает QR-коды, введите следующий секретный ключ: <code>{{ $secret }}</code>
                    2. Получите 6 значный код в приложении и введите его здесь:
                    <form action="{{ route('confirmTwoFactor') }}" method="post">
                        @csrf
                        <input type="text" id="secret-code" name="totp" required>
                        <input type="submit" value="Подтвердить">
                    </form>
                </div>
                <div class="fl clear"></div>
            </div>
        </div>
    </div>
@endsection
