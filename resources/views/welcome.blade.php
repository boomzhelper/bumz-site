<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <title>Bymz.online</title>
    <link rel="stylesheet" href="{{ mix('/css/index.css') }}">
    <link rel="stylesheet" href="{{ mix('/css/news.css') }}">
    <link rel="stylesheet" href="{{ mix('/css/modules/auth.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=960">
    <script>
        function async(u, c) {
            let d = document, t = 'script',
                o = d.createElement(t),
                s = d.getElementsByTagName(t)[0];
            o.src = u;
            if (c) { o.addEventListener('load', function (e) { c(null, e); }, false); }
            s.parentNode.insertBefore(o, s);
        }
    </script>
</head>

<body>
<header>
    <div>
        <div class="pulse">
            <a href=""><img src="img/logo.png" alt=""></a>
        </div>
    </div>
    <nav role="navigation">
        <ul>
            <li><a href="">Главная</a></li>
            <li><a href="">Новости</a></li>
            <li><a href="">Акции</a></li>
            <li><a href="">Магазин</a></li>
            <li><a href="">Бан-лист</a></li>
            <li><a href="">Документы</a></li>
        </ul>
    </nav>
</header>

<main role="main">
    <section class="additional">
    </section>
    <section class="content">
        <div id="slider" class="block slider" data-dynamic="true" data-api="//bumz.local/api">
            <div class="viewport">
                <ul class="slide-wrapper">

                </ul>

                <div class="prev"></div>
                <div class="next"></div>

                <ul class="navigation-buttons">
                </ul>

            </div>
        </div>
        <article class="block news">
            <aside class="ib top">
                <a href="#"><img src="/img/prew_img.jpg" alt="" width="120" height="120"></a>
            </aside>
            <section class="ib top">
                <header>
                    <a href="#">Заголовок новости</a><span>21.01.2017</span>
                </header>
                <p class="news-preview">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                </p>
                <a href="#" class="read-more">
                    <div class="button">Подробнее...</div>
                </a>
            </section>
        </article>
    </section>
    <aside class="menu">
        {{--<div class="border-block" id="userinfo">--}}
            {{--<p id="block-head">--}}
                {{--<span id="userpic"><img src="https://pp.vk.me/c836120/v836120621/ccad/0Eequgd6ZpI.jpg" width="24"></span> Артур Рау--}}
                {{--<span id="userbalance">Баланс-фигламс: <b>99999999999</b>руб.</span>--}}
            {{--</p>--}}
            {{--<div class="user-buttons">--}}
                {{--<div class="user-button">Пополнить счет</div>--}}
                {{--<div class="user-button"><a href="">Играть</a></div>--}}
                {{--<div class="user-button">Какая-то кнопка</div>--}}
                {{--<div class="user-button">Что-то еще</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        @guest
        <div class="block auth">
            <h3 class="heading">Авторизация</h3>
            <form method="POST" action="{{ url('user/login') }}">
                @csrf
                <label for="name">Логин:</label>
                <input type="text" id="name" name="name">
                <label for="password">Пароль:</label>
                <input type="password" id="password" name="password">
                <div class="login-form-buttons">
                    <button type="submit">Вход</button>
                    <button>Регистрация</button>
                </div>
                <div class="fl clear"></div>
            </form>
            <div class="social">
                <p>Или войди через социальные сети:</p>
                <div class="social-icons">
                    <a href="{{ route('social-auth', 'vkontakte') }}"><span><i class="vk"></i></span></a>
                    <a href="{{ route('social-auth', 'facebook') }}"><span><i class="facebook"></i></span></a>
                    <a href="{{ route('social-auth', 'discord') }}"><span><i class="steam"></i></span></a>
                    <span><i class="twitter"></i></span>
                    <a href="{{ route('social-auth', 'yandex') }}"><span><i class="insta"></i></span></a>
                </div>
            </div>
        </div>
        @else
        <div class="block">
            <div class="user-info">
                <img src="{{ url('img/user/avatar.png') }}" alt="User Avatar" class="border">
                <p class="username">{{ Auth::user()->name }}</p>
                <p class="balance">У вас: {{ Auth::user()->points }} ОП. <sup><small>[?]</small></sup></p>
                <div style="clear: both"></div>
            </div>
            <div class="user-navigation">
                <div class="row">
                    <button class="full">Пополнить счет</button>
                </div>
                <div class="row">
                    <button class="">Настройки</button>
                    <button class="" onclick="$('#logout').submit();">Выход</button>
                </div>
            </div>
            <form id='logout' action="{{ url('user/logout') }}" method="get">
                @csrf
            </form>
        </div>
        @endguest
        <div class="block vk no-side-padding">
            <h3 class="heading">Мы ВКонтакте</h3>
            <div id="vk_groups"></div>
        </div>
    </aside>
</main>
<footer>
    <section role="complementary" class="content">
        <h2 class="footer-head">Bymz Online</h2>
        <div class="information">
            <div class="about">
                <a href="//vk.com/">Главный администратор</a>
                <a href="//vk.com/">Администратор</a>
                <a href="//vk.com/">Дозорный [никнейм]</a>
                <a href="//vk.com/">Дозорный [никнейм]</a>
            </div>
            <div class="partners">
                <a href="https://ultravds.com/?p=166" target="_blank"><img src="{{ url('/img/partner/ultravds.png') }}" alt="UltraVDS"></a>
                <a href="http://www.free-kassa.ru/" target="_blank"><img src="http://www.free-kassa.ru/img/fk_btn/31.png" alt="FreeKassa"></a>
            </div>
        </div>
        <div>powered by <a href="//vk.com/omgove" target="_blank">WhiteFang</a>. concept by <a href="//vk.com/leosan" target="_blank">LeoSan</a></div>
    </section>
</footer>
<script>
    async('https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js', function() {
        async('{{ mix('/js/slider.js') }}', function() {
            $('#slider').slider();
        });
    });
    async('https://vk.com/js/api/openapi.js?137', function() {
        VK.Widgets.Group("vk_groups", {
            mode: 3,
            width: "auto"
        }, 119110609);
    });
</script>
<script async="" src="/browser-sync/browser-sync-client.js?v=2.26.5"></script>
@if ($errors->any())
    @foreach ($errors->all() as $error)
        <div>{{$error}}</div>
    @endforeach
@endif
@php var_dump(Auth::user()); echo session()->getId() @endphp
</body>
</html>
