@extends('layouts.app')

@section('title', 'Создание персонажа')

@push('async-css')
    <link rel="stylesheet" href="{{ mix('/css/create.css') }}">
@endpush

@section('content')
    <div class="block character-create">
        <h2>Сосздание персонажа</h2>
        <p class="notice">
            <span>
                P.S. Пол выбирается путем клика по соответствующему персонажу
            </span>
        </p>
        <form id="payment" action="{{ route('create-character') }}" method="POST">
            @csrf
            <div class="group">
                <input class="nickname" name="nickname" type="text" placeholder="никнейм" required>
                <input class="create" type="submit" value="Создать">
            </div>
            <div class="cc-selector sex">
                <input checked="checked" id="visa" type="radio" name="sex" value="female" />
                <label class="drinkcard-cc female left" for="visa"></label>
                <input id="mastercard" type="radio" name="sex" value="male" />
                <label class="drinkcard-cc male right" for="mastercard"></label>
            </div>
            <div class="clear"></div>
        </form>
    </div>
@endsection
