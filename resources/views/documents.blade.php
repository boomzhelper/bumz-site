@extends('layouts.app')

@push('async-scripts')

@endpush

@push('async-css')
    <link rel="stylesheet" href="{{ mix('/css/documents.css') }}">
@endpush

@section('content')
    <div class="block documents">
        <h2 class="h2">Документы</h2>
        <div>
            <p>Здесь собранны все необходимые документы к прочтению :)</p>
            <ol>
                <li><a class="link" href="{{ route('policy') }}">Политика в отношении обработки персональных данных</a></li>
                <li><a class="link" href="{{ route('rules') }}">Правила сервера</a></li>
                <li><a class="link" href="{{ route('cookie') }}">Cookie Policy</a></li>
            </ol>
        </div>
    </div>
@endsection
