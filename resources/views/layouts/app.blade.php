<!DOCTYPE html>
<html lang="ru">

<head>
    @include('modules.head')
    <script>
        function async(u, c) {
            let d = document, t = 'script',
                o = d.createElement(t),
                s = d.getElementsByTagName(t)[0];
            o.src = u;
            if (c) { o.addEventListener('load', function (e) { c(null, e); }, false); }
            s.parentNode.insertBefore(o, s);
        }
    </script>
    <script data-ad-client="ca-pub-6700385844991096" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    @stack('css')
</head>

<body>
<header>
    <div>
        <div class="pulse">
            <a href=""><img src="{{ url("img/logo.png") }}" alt=""></a>
        </div>
    </div>
    <nav role="navigation">
        <ul>
            <li><a href="{{ route('home') }}">Главная</a></li>
            <li><a href="{{ route('news') }}">Новости</a></li>
            <li><a href="{{ route('events') }}">Акции</a></li>
            <li><a href="">Магазин</a></li>
            <li><a href="">Бан-лист</a></li>
            <li><a href="{{ route('documents') }}">Документы</a></li>
        </ul>
    </nav>
</header>

<main role="main">
    <section class="additional">
        @hasSection('additional')
            @yield('additional')
        @endif

        @if((new App\Services\SettingsService)->PaymentBonus() > 0.0)
            <div class="block warning">
                Успейте пополнить счет! На этой неделе действует бонус при пополнении в виде {{ (new App\Services\SettingsService)->PaymentBonus() * 100 }}% от суммы пополнения.
            </div>
        @endif

        @if(Auth::user() && !Auth::user()->registrationCompleted())
            @push('async-css')
                <link rel="stylesheet" href="{{ mix('/css/modules/warning.css') }}">
            @endpush
            <div class="block warning">
                Вы должны закончить регистрацию! У вас отсутствует email-адрес или пароль.
            </div>
        @endif
    </section>
    <section class="content">
        @hasSection('slider')
            @yield('slider')
        @endif
        @yield('content')
        @level(10)
            @include('modules.admin-menu')
        @endlevel
    </section>
    <aside class="menu">
        @guest
            @include('modules.auth')
        @else
            @include('modules.user')
        @endguest
        @permission('merchant.balacne.view')
        <div class="block freekassa">
            <h3 class="heading">FreeKassa.RU</h3>
            @php
            try
            {
                $response = Cache::remember('merchant-balance', 30, function () {
                    $client = new GuzzleHttp\Client();
                    $res = $client->request('GET', 'http://www.free-kassa.ru/api.php?merchant_id=47155&s=f9f8231097247c3e4f0352cfda395042&action=get_balance', [
                        'auth' => ['user', 'pass']
                    ]);
                    $xml = simplexml_load_string($res->getBody());
                    return (string)$xml->balance;
                });
            }
            catch (Exception $exception) {
                $response = $exception->getMessage();
            }
            @endphp
            <div>Баланс: {{ $response }} руб.</div>
        </div>
        @endpermission
        <div class="block tg no-side-padding">
            <h3 class="heading">Телеграм канал</h3>
            <iframe id="tgw_5eb06cf583ba8846658b4569" frameborder="0" scrolling="no" horizontalscrolling="no" verticalscrolling="no" width="100%" height="320px" async></iframe>
            <script>
                document.addEventListener("DOMContentLoaded",function(){document.getElementById("tgw_5eb06cf583ba8846658b4569").setAttribute("src","https://tgwidget.com/channel/v2.0/?id=5eb06cf583ba8846658b4569")})
            </script>
        </div>
        <div class="block vk no-side-padding">
            <h3 class="heading">Мы ВКонтакте</h3>
            <div id="vk_groups"></div>
        </div>
        <div class="block server-online" data-request="{{ config("game.request") }}">
            <h3 class="heading">Онлайн на сервере</h3>
            <div class="content">

            </div>
        </div>
    </aside>
    <div style="clear: both"></div>
</main>
<footer>
    <section role="complementary" class="content">
        <h2 class="footer-head">RussianBomb</h2>
        <div class="information">
            <div class="about">
                <a href="//vk.com/omgove">Администратор №1</a>
                <a href="//vk.com/id576983693">Администратор №2</a>
            </div>
            <div class="partners">
                <a href="https://ultravds.com/?p=166" target="_blank"><img src="{{ url('/img/partner/ultravds.png') }}" alt="UltraVDS"></a>
                <a href="http://www.free-kassa.ru/" target="_blank"><img src="//www.free-kassa.ru/img/fk_btn/31.png" alt="FreeKassa"></a>
            </div>
        </div>
        <div>powered by <a href="//vk.com/omgove" target="_blank">WhiteFang</a>. design by <a href="//vk.com/feveryi" target="_blank">Ace</a></div>
    </section>
</footer>
<script>
    async('https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js', function() {
        @stack('async-scripts')
    });
    async('https://vk.com/js/api/openapi.js?137', function() {
        VK.Widgets.Group("vk_groups", {
            mode: 3,
            width: "268"
        }, {{ config('vkontakte.community_id') }});
    });
    async('{{mix('js/server.online.js')}}', function() {
        $('.server-online').online();
    });
    async('https://cdn.jsdelivr.net/npm/sweetalert2@8', function() {
        @if ($errors->any())
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: '@foreach ($errors->all() as $error){!! __($error) !!} @endforeach',
            animation: false,
            customClass: {
                popup: 'animated tada'
            }
        });
        @endif
        @if (\Session::has('success'))
        Swal.fire({
            type: 'success',
            title: 'Well done',
            text: '@foreach (\Session::get('success') as $msg){!! __($msg)  !!} @endforeach',
            animation: false,
            customClass: {
                popup: 'animated tada'
            }
        });
        @endif
    });
</script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
@guest
    <link rel="stylesheet" href="{{ mix('/css/modules/auth.css') }}">
@else
    <link rel="stylesheet" href="{{ mix('/css/modules/user.css') }}">
@endguest
@stack('async-css')
{!!  GoogleReCaptchaV3::init() !!}
</body>
</html>
