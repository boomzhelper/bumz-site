@extends('layouts.app')

@section('title', 'Завершение регистрации')

@push('async-css')
    <link rel="stylesheet" href="{{ mix('/css/registration.css') }}">
@endpush

@push('async-scripts')
    async('{{ mix('/js/validator.js') }}', function() {
        $('#registration').validator();
    });
@endpush

@section('content')
    <div class="block registration">
        <h2>Завершение регистрации</h2>
        <form id="registration" action="{{ route('registration-completing') }}" method="POST">
            @csrf
            @if(is_null(Auth::user()->email))
            <div class="row">
                <div class="form-group has-feedback">
                    <input data-remote="/validation/registration" data-pattern-error="Email адрес не соотвуствует правилам" data-error="Данный email адрес уже занят" data-required-error="Введите действующий адрес электронной почты" type="email" name="email" placeholder="E-mail" required>
                    <div class="tip">
                        <p class="outer">
                            <span class="inner help-block with-errors">
                                <span class="empty">Введите действующий адрес электронной почты</span>
                                <span class="success">Email адрес свободен и вы можете использовать его!</span>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
            @endif
            @if(is_null(Auth::user()->password))
            <div class="row">
                <div class="form-group has-feedback">
                    <input type="password" name="password" placeholder="Пароль" id="pwd" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" required>
                    <div class="tip">
                        <p class="outer">
                            <span class="inner help-block with-errors">
                                <span class="empty">Придумайте пароль</span>
                                <span class="success">Пароль идеально подходит :)</span>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group has-feedback">
                    <input data-match="#pwd" type="password" name="password_confirmation" placeholder="Повторите пароль" required>
                    <div class="tip">
                        <p class="outer">
                            <span class="inner help-block with-errors">
                                <span class="empty">Введите еще раз придуманный пароль</span>
                                <span class="success">Все верно!</span>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
            @endif
            <div class="row">
                <hr>
            </div>
            <div class="row">
                <div class="form-group">
                    <p class="user-agreement">Зарегистрировавшись, вы автоматически принимаете
                        <a href="#" target="_blank">пользовательское соглашение</a>
                        и <a href="#">политику конфиденциальности</a> нашего сервиса.</p>
                    <input class="registration-button" type="submit" value="Завершить регистрацию">
                </div>
            </div>
        </form>
    </div>
@endsection
