const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.stylus('resources/stylus/completing/index.styl', 'public/css');
mix.stylus('resources/stylus/completing/settings.styl', 'public/css');
mix.stylus('resources/stylus/completing/payment.styl', 'public/css');
mix.stylus('resources/stylus/completing/news.styl', 'public/css');
mix.stylus('resources/stylus/completing/registration.styl', 'public/css');
mix.stylus('resources/stylus/completing/slide.create.styl', 'public/css');
mix.stylus('resources/stylus/completing/enableTwoFactor.styl', 'public/css');
mix.stylus('resources/stylus/completing/validateTwoFactor.styl', 'public/css');
mix.stylus('resources/stylus/completing/post-create.styl', 'public/css');
mix.stylus('resources/stylus/completing/events.styl', 'public/css');
mix.stylus('resources/stylus/completing/documents.styl', 'public/css');

mix.stylus('resources/stylus/character/create.styl', 'public/css');

mix.stylus('resources/stylus/modules/auth.styl', 'public/css/modules');
mix.stylus('resources/stylus/modules/user.styl', 'public/css/modules');
mix.stylus('resources/stylus/modules/warning.styl', 'public/css/modules');
mix.stylus('resources/stylus/modules/slider.styl', 'public/css/modules');
mix.stylus('resources/stylus/modules/dropdown.styl', 'public/css/modules');
mix.stylus('resources/stylus/modules/pagination.styl', 'public/css/modules');

mix.scripts('resources/js/slider.js', 'public/js/slider.js');
mix.scripts('resources/js/validator.js', 'public/js/validator.js');
mix.scripts('resources/js/server.online.js', 'public/js/server.online.js');
mix.scripts('resources/js/admin-slide-create.js', 'public/js/admin-slide-create.js');
mix.scripts('resources/js/jquery.inputmask.js', 'public/js/jquery.inputmask.js');

// if (mix.inProduction()) {
//     mix.version();
// }
mix.version();
mix.browserSync('bumz.local');
