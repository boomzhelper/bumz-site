(function ($) {

    const methods = {
        readURL: function () {
            let input = $(this).find('.uploader');
            if (input.prop('files') && input.prop('files')[0]) {
                let reader = new FileReader();

                reader.onload = function(e) {
                    $(methods['sliderCreateForm']+' + img').remove();
                    $(methods['sliderCreateForm']).after('<img src="'+e.target.result +'" class="block preview" width="650" height="350"/>');
                };

                reader.readAsDataURL(input.prop('files')[0]);
            }
        },

        init: function (options) {
            let $this = $(this),
                data = $this.data('slidePreviewer');

            if (!data) {

                methods['sliderCreateForm'] = $this.data('form');

                let input = $(this).find('.uploader');
                $(this).find('.uploader').change(function() {
                    methods['readURL'].apply($this);
                });
            }

        },

        sliderCreateForm: ""
    };

    $.fn.slidePreviewer = function(method) {
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метод с именем ' +  method + ' не существует для jQuery.slidePreviewer' );
        }
    };

})(jQuery);
