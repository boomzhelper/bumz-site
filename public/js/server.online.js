(function($) {

    const methods = {
        init: function(options) {
            return this.each(function(){
                let $this = $(this),
                    data = $this.data('online');
                // Если плагин ещё не проинициализирован
                if (!data) {
                    let cont = $this.find('.content');
                    $.get($this.data('request') + "/ServerList.ashx?rnd=" + Math.floor(Math.random() * Math.floor(999999)), null, function (data) {
                        let xmlDoc = $.parseXML( data ),
                            $xml = $( xmlDoc ),
                            $result = $xml.find( "Result" );

                        $('<span>Онлайн: '+ $result.attr('total') +'</span>').appendTo(cont);
                    });
                }
            });
        },
        destroy: function() {

            return this.each(function(){

                let $this = $(this),
                    data = $this.data('online');

                // пространства имён рулят!!11
                //$(window).unbind('.tooltip');
                //data.tooltip.remove();
                $this.removeData('online');

            })

        },
    };

    $.fn.online = function(method) {
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метод с именем ' +  method + ' не существует для jQuery.online' );
        }
    };
})(jQuery);
