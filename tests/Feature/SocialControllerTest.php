<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SocialControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testRedirectToYandex()
    {
        $response = $this->get('/social/yandex/redirect');

        //$response->dump();

        $response->assertStatus(302);
    }

    public function testRedirectToVK()
    {
        $response = $this->get('/social/vkontakte/redirect');

        //$response->dump();

        $response->assertStatus(302);
    }

    public function testRedirectToDiscord()
    {
        $response = $this->get('/social/discord/redirect');

        //$response->dump();

        $response->assertStatus(302);
    }
}
